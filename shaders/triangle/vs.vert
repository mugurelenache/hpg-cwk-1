#version 450
#extension GL_ARB_separate_shader_objects : enable

/////////////// STRUCTS ///////////////////////
struct TRANSFORM
{
    vec4 position;
    vec4 rotation;
    vec4 scale;
    mat4 matrix;
};

struct CAMERA_PROJECTION
{
    mat4 matrix;
};
////////////////////////////////////////////////

/////////////////// VS INPUT ///////////////////
layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec4 inColor;
layout(location = 2) in vec4 inNormal;
layout(location = 3) in vec2 inUV;
////////////////////////////////////////////////

////////////////// UNIFORMS ////////////////////
layout(set = 0, binding = 0) uniform CAMERA_TRANSFORM_BLOCK
{
    TRANSFORM transform;
} camera;

layout(set = 0, binding = 1) uniform CAMERA_PROJECTION_BLOCK
{
    CAMERA_PROJECTION projection;
} camera_p;

layout(set = 1, binding = 0) uniform LIGHT_TRANSFORM_BLOCK
{
    TRANSFORM transform;
} light;

layout(set = 2, binding = 0) uniform ACTOR_BLOCK
{
    TRANSFORM transform;
} actor;
////////////////////////////////////////////////

///////////////// VS OUTPUTS /////////////////// 
layout(location = 0) out vec3 outFragPos;
layout(location = 1) out vec4 outFragColor;
layout(location = 2) out vec3 outFragNormal;
layout(location = 3) out vec2 outUV;
layout(location = 4) out vec3 outLightPos;
layout(location = 5) out vec3 outCameraPos;
////////////////////////////////////////////////

void main()
{
    gl_Position = camera_p.projection.matrix * camera.transform.matrix * actor.transform.matrix * inPosition;

    outFragPos = vec3(actor.transform.matrix * inPosition);
    outFragColor = inColor;
    outFragNormal = normalize((transpose(inverse(actor.transform.matrix)) * inNormal).xyz);
    outUV = inUV;
    outLightPos = normalize(light.transform.position.xyz - outFragPos);
    outCameraPos = normalize(camera.transform.position.xyz - outFragPos);
}