#pragma once
#include <vulkan/vulkan.h>
#include "Buffer.h"
#include "imgui.h"
#include <glm/glm.hpp>

// Forward decl
class Application;

/**
 * @brief UI Overlay class
 *
 * Includes the UI functionality for vk, as well as the frontend
*/
class UIOverlay
{
public:
	// Push constants for imgui
	struct PushConstantsBlock
	{
		glm::vec2 scale = glm::vec2(1.0f);
		glm::vec2 translate = glm::vec2(0.0f);
	};

public:
	// Buffers for vtx and idx
	std::vector<Buffer> vertexBuffers;
	std::vector<Buffer> indexBuffers;

	// Counts for the old lengths of the buffers
	std::vector<int> oldVertexCounts;
	std::vector<int> oldIndexCounts;

	// Vulkan Specific members
	VkPipeline uiGraphicsPipeline = VK_NULL_HANDLE;
	VkPipelineLayout uiGraphicsPipelineLayout = VK_NULL_HANDLE;

	std::vector<VkDescriptorSet> descriptorSets;
	std::vector<VkDescriptorSetLayout> descriptorSetLayouts;

	PushConstantsBlock pushConstantsBlock;
	VkImage fontTextureImage = VK_NULL_HANDLE;
	VkDeviceMemory fontTextureMemory = VK_NULL_HANDLE;
	VkImageView fontTextureView = VK_NULL_HANDLE;
	VkSampler fontTextureSampler = VK_NULL_HANDLE;

	VkDescriptorPool descriptorPool = VK_NULL_HANDLE;

public:
	// Constructor
	UIOverlay();

	// Destroy the resources
	void destroy();

	// Initializes the ui overlay
	void initialize();

	// Loads imgui fonts & creates descriptor sets
	void acquireResources();

	// Creates the descriptor pool
	void createDescriptorPool();

	// Prepares the descriptor sets & layouts & bindings
	void prepareDescriptorSets();

	// Prepares the pipeline
	void preparePipeline(const VkPipelineCache& pipelineCache, const VkRenderPass& renderPass);

	// Returns whether the update on the buffers has been made
	bool update();

	// Updates a buffer linked to a frame
	bool updateBuffersOnFrame(const uint32_t& frameID);

	// Records the drawing commands
	void draw(const VkCommandBuffer& commandBuffer, const uint32_t& frameID, const VkFramebuffer& framebuffer);

	// Front-end method
	void newFrame(Application& application);
};

