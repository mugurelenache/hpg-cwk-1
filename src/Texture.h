#pragma once
#include <vulkan/vulkan.h>
#include <string>

/**
 * @brief Texture class
 * Includes memory associated, sampler, image & image views
*/
class Texture
{
public:
	int width = 0;
	int height = 0;
	int channels = 0;

	VkImage textureImage = VK_NULL_HANDLE;
	VkImageView textureImageView = VK_NULL_HANDLE;
	VkSampler textureImageSampler = VK_NULL_HANDLE;
	VkDeviceMemory textureImageMemory = VK_NULL_HANDLE;
	uint32_t textureImageMipLevels = 0;

public:
	// Defaulted constructor
	Texture() = default;

	// Method to load a texture
	void load(const std::string& path);

	// Method to destroy the resources
	void destroy();
};