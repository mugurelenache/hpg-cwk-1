#include "Vertex.h"

/**
 * @brief Default vertex constructor
*/
Vertex::Vertex()
{
	pos = glm::vec4(0.0f);
	color = glm::vec4(0.0f);
	normal = glm::vec4(0.0f);
	texCoord = glm::vec2(0.0f);
}


/**
 * @brief Vertex constructor with params
 * @param pos
 * @param color
 * @param normal
 * @param texCoord
*/
Vertex::Vertex(const glm::vec4& pos, const glm::vec4& color, const glm::vec4& normal, const glm::vec2& texCoord)
{
	this->pos = pos;
	this->color = color;
	this->normal = normal;
	this->texCoord = texCoord;
}


/**
 * @brief Gets the attribute description of such data structure
 * @return
*/
std::vector<VkVertexInputAttributeDescription> Vertex::getAttributeDescriptions()
{
	std::vector<VkVertexInputAttributeDescription> attributeDescriptions(4);

	attributeDescriptions[0].binding = 0;
	attributeDescriptions[0].location = 0;
	attributeDescriptions[0].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	attributeDescriptions[0].offset = offsetof(Vertex, pos);

	attributeDescriptions[1].binding = 0;
	attributeDescriptions[1].location = 1;
	attributeDescriptions[1].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	attributeDescriptions[1].offset = offsetof(Vertex, color);

	attributeDescriptions[1].binding = 0;
	attributeDescriptions[2].location = 2;
	attributeDescriptions[2].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	attributeDescriptions[2].offset = offsetof(Vertex, normal);

	attributeDescriptions[3].binding = 0;
	attributeDescriptions[3].location = 3;
	attributeDescriptions[3].format = VK_FORMAT_R32G32_SFLOAT;
	attributeDescriptions[3].offset = offsetof(Vertex, texCoord);

	return attributeDescriptions;
}


/**
 * @brief Operator overload, comparing vertices
 * @param other
 * @return
*/
bool Vertex::operator==(const Vertex& other) const
{
	return pos == other.pos && color == other.color && texCoord == other.texCoord;
}


/**
 * @brief Gets the binding descriptions
 * @return
*/
VkVertexInputBindingDescription Vertex::getBindingDescription()
{
	VkVertexInputBindingDescription bindingDescription{};
	bindingDescription.binding = 0;
	bindingDescription.stride = sizeof(Vertex);
	bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	return bindingDescription;
}