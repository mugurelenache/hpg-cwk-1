#pragma once
#include "Vertex.h"
#include "Buffer.h"
#include <filesystem>
#include "Material.h"

namespace fs = std::filesystem;

/**
 * @brief Mesh class
 *
 * This is a wrapper for the vertex & index buffer
 * with an assigned material
*/
class Mesh
{
public:
	std::vector<Vertex> vertices;
	std::vector<uint32_t> indices;

	Material material;

	Buffer vertexBuffer;
	Buffer indexBuffer;

public:
	// Defaulted constructor
	Mesh() = default;

	// Constructor with vertex & index buffer
	Mesh(const std::vector<Vertex>& vertices, const std::vector<uint32_t>& indices);

	// Method to load an obj
	static std::vector<Mesh> loadObj(const fs::path& path);

	// Method to generate a single triangle
	static Mesh generateTriangle();

	// Method to generate the buffers from vertices & indices
	void generateBuffers();

	// Method to destroy the buffers and the material associated
	void destroy();
};