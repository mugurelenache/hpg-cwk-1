#pragma once
#include "QueueFamilyIndices.h"
#include <vulkan/vulkan.h>
#include "Buffer.h"
#include "ShaderCompiler.h"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

/**
 * @brief Singleton, data class only
 *
 * Mainly contains the variables that are needed
 * in most of the classes.
*/
class VKC
{
public:
	ShaderCompiler shaderCompiler;
	VkDevice device = VK_NULL_HANDLE;
	VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
	VkCommandPool commandPool = VK_NULL_HANDLE;

	QueueFamilyIndices queueFamilies;

	VkQueue graphicsQueue = VK_NULL_HANDLE;
	VkQueue transferQueue = VK_NULL_HANDLE;
	VkQueue presentQueue = VK_NULL_HANDLE;

	VkSwapchainKHR swapchain = VK_NULL_HANDLE;
	std::vector<VkImage> swapchainImages;
	std::vector<VkImageView> swapchainImageViews;
	VkFormat swapchainImageFormat = VK_FORMAT_UNDEFINED;
	VkExtent2D swapchainExtent = { };

	VkDescriptorPool descriptorPool = VK_NULL_HANDLE;

	std::vector<Buffer> uniformBuffers;
	VkDeviceSize minUniformBufferOffsetAlignment = 1;

	std::string texturesPath = "assets";
	const std::string shadersPath = "shaders";
	const std::string currentShaderPath = "triangle";
	const std::string modelsPath = "assets";
	const std::string objectName = "duck/duck.obj";

	GLFWwindow* window;
	int windowWidth = 1024;
	int windowHeight = 720;

	VkSampleCountFlagBits msaaSamples;

private:
	VKC() = default;

public:
	// Singleton getter
	static VKC& get()
	{
		static VKC instance;
		return instance;
	};

	// Deleted copy constructor
	VKC(const VKC&) = delete;

	// Deleted operator =
	void operator=(const VKC&) = delete;
};

