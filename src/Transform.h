#pragma once
#include <glm/glm.hpp>
#include <vulkan/vulkan.h>

/**
 * @brief Transform class
 *
*/
class Transform
{
public:
	glm::vec4 position = glm::vec4(0.0f);
	glm::vec4 rotation = glm::vec4(0.0f);
	glm::vec4 scale = glm::vec4(1.0f);
	glm::mat4 matrix = glm::mat4(1.0f);

	// Vulkan specific
	VkDeviceSize offsetIntoBuffer = 0;

public:
	// Constructor
	Transform() = default;

	// Applies the transformations to generate a matrix
	void applyTRS();
};

