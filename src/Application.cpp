#include "Application.h"
#include <iostream>
#include <vulkan/vulkan_core.h>
#include <map>
#include <set>
#include <filesystem>
#include "VKC.h"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#ifndef TINYOBJLOADER_IMPLEMENTATION
#define TINYOBJLOADER_IMPLEMENTATION
#endif // !TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

namespace fs = std::filesystem;

#ifndef MAIN_DIR
#define MAIN_DIR std::filesystem::current_path()
#endif

#include "Utility.h"

/**
 * @brief Constructor
 *
 * Initializes the Window
 * Initializes Vulkan
*/
Application::Application()
{
	initializeWindow();
	initializeVulkan();
}

/**
 * @brief Destructor
 *
 * Destroys the components in the reverse creation order
*/
Application::~Application()
{
	// Destroys all the synchronization primitives for each frame
	for (size_t i = 0; i < maxFramesInFlight; i++)
	{
		// Destroy render-fin semaphore
		vkDestroySemaphore(VKC::get().device, renderFinishedSemaphores[i], nullptr);
		// Destroy image-available semaphore
		vkDestroySemaphore(VKC::get().device, imageAvailableSemaphores[i], nullptr);
		// Destroy fences
		vkDestroyFence(VKC::get().device, inFlightFences[i], nullptr);
	}

	uiOverlay.destroy();

	// Destroy graphics pipeline layout
	vkDestroyPipelineLayout(VKC::get().device, graphicsPipelineLayout, nullptr);
	// Destroys the pipeline
	vkDestroyPipeline(VKC::get().device, graphicsPipeline, nullptr);
	// Cleans up the old swapchain
	cleanupOldSwapchain();

	destroyUserData();

	// Destroys the command pool
	vkDestroyCommandPool(VKC::get().device, VKC::get().commandPool, nullptr);
	// Destroys the surface
	vkDestroyDevice(VKC::get().device, nullptr);
	// Destroys the logical device
	vkDestroySurfaceKHR(instance, surface, nullptr);
	// Destroys the vulkan instance
	if (debug)
	{
		// Destroys the debug messenger
		Utility::destroyDebugMessenger(instance, debugMessenger);
	}
	// Destroys the instance
	vkDestroyInstance(instance, nullptr);
	// Destroys the window
	glfwDestroyWindow(VKC::get().window);
	// Stops the GLFW
	glfwTerminate();
}


/**
 * @brief The main loop of the application
 *
 * Polls GLFW events and draws the scene
*/
void Application::mainLoop()
{
	// As long as the window is not closing
	while (!glfwWindowShouldClose(VKC::get().window))
	{
		glfwPollEvents();
		draw();
	}

	// Wait for the last commands to end before destroying everything
	vkDeviceWaitIdle(VKC::get().device);
}


/**
 * @brief Initializes vulkan and the UI
*/
void Application::initializeVulkan()
{
	createInstance();
	createSurface();
	pickPhysicalDevice();
	createLogicalDevice();
	createCommandPool();

	createSwapchain();
	createImageViews();
	createRenderpass();
	createDescriptorPool();

	loadModel();
	createUniformBuffers();
	mapUserDataToBuffers();
	generateUserDataDSL();

	createGraphicsPipeline();
	createColorResources();
	createDepthResources();
	createFramebuffers();

	// UI Initialization Here
	uiOverlay.initialize();
	uiOverlay.createDescriptorPool();
	uiOverlay.acquireResources();
	uiOverlay.prepareDescriptorSets();
	uiOverlay.preparePipeline(VK_NULL_HANDLE, renderPass);

	createCommandBuffers();
	createSyncObjects();

	sceneInit();
}


/**
 * @brief Initializes GLFW Window and sets up the callback functions
*/
void Application::initializeWindow()
{
	// Initializes GLFW
	glfwInit();
	// Sets the window hints
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_DOUBLEBUFFER, GL_FALSE);

	std::string combinedTitle = TITLE + " - " + VERSION_ID;

	// Creates the window
	VKC::get().window = glfwCreateWindow(VKC::get().windowWidth, VKC::get().windowHeight, combinedTitle.c_str(), nullptr, nullptr);
	glfwSetWindowUserPointer(VKC::get().window, this);
	glfwSetFramebufferSizeCallback(VKC::get().window, framebufferResizeCallback);
}


/**
 * @brief Function that gets called whenever the window size changes
 * @param window
 * @param width
 * @param height
*/
void Application::framebufferResizeCallback(GLFWwindow* window, int width, int height)
{
	auto applicationRef = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));
	VKC::get().windowWidth = width;
	VKC::get().windowHeight = height;
	applicationRef->framebufferResized = true;
}


/**
 * @brief Initializes the scene [usually few transforms done]
*/
void Application::sceneInit()
{
	// Camera
	camera.transform.position = glm::vec4(0.0f, 0.0f, -2.0f, 1.0f);
	camera.transform.matrix = glm::lookAt(glm::vec3(camera.transform.position), glm::vec3(0.0f), glm::vec3(0.0f, -1.0f, 0.0f));
	camera.projection = glm::perspective(glm::radians(45.0f), VKC::get().swapchainExtent.width / static_cast<float>(VKC::get().swapchainExtent.height), 0.1f, 100.f);

	// Light
	light.transform.position = glm::vec4(0.0f, 5.0f, 0.0f, 1.0f);
	light.transform.applyTRS();

	// Actor
	duckActor.transform.scale = glm::vec4(0.01f, 0.01f, 0.01f, 1.0f);
	duckActor.transform.rotation = glm::vec4(-90.0f, 0.0f, 180.0f, 1.0f);
	duckActor.transform.applyTRS();
}


/**
 * @brief Creates the Vulkan Instance
 *
 * Queries GLFW for additional instance extensions
*/
void Application::createInstance()
{
	// Checks if all required validation layers exist
	bool hasAllRequiredValidationLayers = checkValidationLayers();

	if (debug && !hasAllRequiredValidationLayers)
	{
		std::cerr << "Error: Could not find the requested validation layers." << std::endl;
		std::exit(-1);
	}

	// Filling the application info structure with application details
	VkApplicationInfo applicationInfo = {};
	applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	applicationInfo.pApplicationName = TITLE.c_str();
	applicationInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	applicationInfo.pEngineName = "MUGU_ENGINE";
	applicationInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	applicationInfo.apiVersion = VK_API_VERSION_1_2;

	// Filling the vulkan instance create info
	// we ensure that we add the enabled required validation layers
	VkInstanceCreateInfo instanceCI = {};
	instanceCI.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instanceCI.pApplicationInfo = &applicationInfo;
	instanceCI.enabledLayerCount = static_cast<uint32_t>(requiredValidationLayers.size());
	instanceCI.ppEnabledLayerNames = requiredValidationLayers.data();

	// Sets the extension ptr to messengerCI
	auto messengerCI = Utility::generateMessengerCreateInfo();
	if (debug)
	{
		instanceCI.pNext = (VkDebugUtilsMessengerCreateInfoEXT*)&messengerCI;
	}

	// Query extensions (we need swapchain ext + debug)
	const auto extensions = getExtensions();

	// Ensure that we add those to the instance create info
	instanceCI.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
	instanceCI.ppEnabledExtensionNames = extensions.data();

	// Try to create the instance
	if (vkCreateInstance(&instanceCI, nullptr, &instance) != VK_SUCCESS)
	{
		std::cerr << "Error: Vulkan Instance cannot be created." << std::endl;
		std::exit(-1);
	}

	// Create the debug messenger if debug mode is on
	if (debug)
	{
		Utility::createDebugMessenger(instance, &messengerCI, debugMessenger);
	}
}


/**
 * @brief Checks if the requested validations layers exist
 * @return Whether all layers exist
*/
bool Application::checkValidationLayers()
{
	// Enumerate and save the available instance validation layers of our device & driver
	uint32_t layerCount = 0;
	vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
	std::vector<VkLayerProperties> instanceLayers(layerCount);
	vkEnumerateInstanceLayerProperties(&layerCount, instanceLayers.data());

	// Iterate through our required layers to see if we have them available
	for (const auto& requiredLayer : requiredValidationLayers)
	{
		bool found = true;

		// Iterate through the available layers
		for (const auto& layer : instanceLayers)
		{
			// If we found the required layer we continue to
			// the next required layer
			if (layer.layerName == requiredLayer)
			{
				found = true;
				break;
			}
		}

		if (!found)
		{
			return false;
		}
	}

	return true;
}


/**
 * @brief Queries GLFW for extensions & adds extra debug exts
 * @return vector of extensions
*/
std::vector<const char*> Application::getExtensions()
{
	// Enumerate the required instance extensions and save them
	uint32_t glfwExtCount = 0;
	const char** glfwExtensions;
	glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtCount);

	// Create an array of extensions
	std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtCount);

	// Add the debug utils extensions for debug mode
	if (debug)
	{
		extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	}

	return extensions;
}


/**
 * @brief Picks the physical device given the rating of all the devices
*/
void Application::pickPhysicalDevice()
{
	// Enumerate all the devices
	uint32_t phyDeviceCount = 0;
	vkEnumeratePhysicalDevices(instance, &phyDeviceCount, nullptr);

	// We exit if there's no device.
	if (phyDeviceCount == 0)
	{
		std::cerr << "Error: Failed to find any Vulkan-compatible device." << std::endl;
		std::exit(-1);
	}

	// Store physical devices into the array
	std::vector<VkPhysicalDevice> phyDevices(phyDeviceCount);
	vkEnumeratePhysicalDevices(instance, &phyDeviceCount, phyDevices.data());

	// Create a sorting map [from high to low]
	std::multimap<uint32_t, VkPhysicalDevice, std::greater<>> devicesMap;

	// Get the best available device (simple max-search)
	size_t id = static_cast<size_t>(-1);
	uint32_t maxScore = 0;

	// Calculate the score of each physical device
	// then add it into the map
	for (const auto& phyDevice : phyDevices)
	{
		uint32_t tempScore = rateDevice(phyDevice);
		devicesMap.insert(std::make_pair(tempScore, phyDevice));
	}

	// Carefully check each device if they're adequate
	// and if they support all the requested features
	// The first one that is found and has those, will be chosen
	for (auto& elem : devicesMap)
	{
		// Gets the queue families for that device
		// This would mean a Graphics Queue, Present Queue, Transfer Queue
		QueueFamilyIndices familyIndices = findQueueFamilies(elem.second);

		// Checks if the device contains the required extensions
		bool containsExtensions = checkDeviceExtensionsSupport(elem.second);
		SwapchainSupportDetails swapchainSupportDetails = querySwapchainSupport(elem.second);
		bool isAdequate = !swapchainSupportDetails.formats.empty() && !swapchainSupportDetails.presentModes.empty();

		// Query the features of that device
		VkPhysicalDeviceFeatures supportedFeatures;
		vkGetPhysicalDeviceFeatures(elem.second, &supportedFeatures);

		// If all the conditions are satisfied, we choose the device
		if (familyIndices.isComplete() && containsExtensions && isAdequate && supportedFeatures.samplerAnisotropy)
		{
			VKC::get().physicalDevice = elem.second;
			break;
		}
	}

	// Set the global queue families
	VKC::get().queueFamilies = findQueueFamilies(VKC::get().physicalDevice);

	// We couldn't find a physical device
	if (VKC::get().physicalDevice == VK_NULL_HANDLE)
	{
		std::cerr << "Error: Cannot find a valid physical device." << std::endl;
		std::exit(-1);
	}

	// Populate the MSAA maximum samples
	VKC::get().msaaSamples = Utility::getMaxUsableSampleCount();
}


/**
 * @brief Rates the device depending on its type and features
 * @param device
 * @return a score of that device
*/
uint32_t Application::rateDevice(const VkPhysicalDevice& device)
{
	uint32_t score = 0;

	// Query and save the device properties
	VkPhysicalDeviceProperties deviceProperties;
	vkGetPhysicalDeviceProperties(device, &deviceProperties);

	// Query and save the device features
	VkPhysicalDeviceFeatures deviceFeatures;
	vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

	// We mainly look for a GPU
	// The device gets a lower score if it's an integrated / virtual gpu / cpu
	if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
	{
		score += 1000;
	}
	else if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU)
	{
		score += 500;
	}
	else if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU)
	{
		// Virtual node in a VENV
		score += 250;
	}
	else
	{
		// OTHER / CPU
		score += 100;
	}

	// And for max image dimension 2d (can represent resolution)
	score += deviceProperties.limits.maxImageDimension2D;

	return score;
}


/**
 * @brief Gets the queue families
 * @param device
 * @return
*/
QueueFamilyIndices Application::findQueueFamilies(const VkPhysicalDevice& device)
{
	// Query the queue family properties and store them
	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

	// Temporary indices struct
	QueueFamilyIndices indices;

	// Iterate through the queue families properties
	// and simply bit-check if the queue family
	for (size_t i = 0; i < queueFamilies.size(); i++)
	{
		// Graphics Family Check
		if (!indices.graphicsFamily.has_value())
		{
			if (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
			{
				indices.graphicsFamily = static_cast<uint32_t>(i);
			}
		}

		// Transfer Family Check
		if (!indices.transferFamily.has_value())
		{
			if (queueFamilies[i].queueFlags & VK_QUEUE_TRANSFER_BIT)
			{
				indices.transferFamily = static_cast<uint32_t>(i);
			}
		}

		// Present Family Check
		if (!indices.presentFamily.has_value())
		{
			VkBool32 presentSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(device, static_cast<uint32_t>(i), surface, &presentSupport);

			if (presentSupport)
			{
				indices.presentFamily = static_cast<uint32_t>(i);
			}
		}

		// Return on true
		if (indices.isComplete())
		{
			break;
		}
	}

	return indices;
}

/**
 * @brief Checks whether the required device extensions are available on a device
 * @param device
 * @return whether the device contains all required extensions
*/
bool Application::checkDeviceExtensionsSupport(const VkPhysicalDevice& device)
{
	// Query and store the extensions into the available extensions
	uint32_t extensionCount;
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);
	std::vector<VkExtensionProperties> availableExtensions(extensionCount);
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

	// Create a set of unique extensions
	std::set<std::string> requiredExtensions(requiredDeviceExtensions.begin(), requiredDeviceExtensions.end());

	// Iterate through the available one and delete the required extension
	// from the original array
	for (const auto& extension : availableExtensions)
	{
		requiredExtensions.erase(extension.extensionName);
	}

	// If the array is empty, it returns true
	// this means that we have all the extensions necessary
	return requiredExtensions.empty();
}


/**
 * @brief Queries the swapchain support
 *
 * It mainly looks for available surface formats, present modes, capabilities
 *
 * @param device
 * @return
*/
SwapchainSupportDetails Application::querySwapchainSupport(const VkPhysicalDevice& device)
{
	SwapchainSupportDetails details;

	// Querying the physical device capabilities
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

	// Query the physical device formats
	uint32_t formatsCount = 0;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatsCount, nullptr);

	// Store them if existent
	if (formatsCount)
	{
		details.formats.resize(formatsCount);
		// Store them
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatsCount, details.formats.data());
	}

	// Query the physical device present modes
	uint32_t presentModesCount = 0;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModesCount, nullptr);

	// Store them if existent
	if (presentModesCount)
	{
		details.presentModes.resize(presentModesCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModesCount, details.presentModes.data());
	}

	return details;
}


/**
 * @brief Creates a logical device given a physical device
*/
void Application::createLogicalDevice()
{
	// Populate the temporary indices with the existing queue families
	QueueFamilyIndices indices = VKC::get().queueFamilies;

	// Create a set of unique queue families
	// Some of them can repeat (e.g. graphics might have transfer capability)
	std::set<uint32_t> uniqueQueueFamilies = { indices.graphicsFamily.value(), indices.presentFamily.value(), indices.transferFamily.value() };

	// Array of queue create infos
	// will be used in the creation of the logical device
	std::vector<VkDeviceQueueCreateInfo> queueCIs;

	// highest priority
	float queuePriority = 1.0f;
	// Add the individual queues CI to a vector of queues CIs
	for (uint32_t queueFamilyID : uniqueQueueFamilies)
	{
		// Create a queue create info
		// It represents one of the families (graphics/transfer/present)
		// And has maximum priority
		VkDeviceQueueCreateInfo queueCI = {};
		queueCI.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCI.queueFamilyIndex = queueFamilyID;
		queueCI.queueCount = 1;
		queueCI.pQueuePriorities = &queuePriority;
		// Append it to the vector
		queueCIs.push_back(queueCI);
	}

	// Mark the device feature sample anisotropy to be true
	VkPhysicalDeviceFeatures deviceFeatures{};
	deviceFeatures.samplerAnisotropy = VK_TRUE;

	// Populate the logical device struct:
	//
	// We are looking to create the required queues for our tasks
	// Enable the validation layers we required
	// As well as the extensions required
	//
	// We also set the enabled features to be a ptr to ours ^ (for anisotropy)
	VkDeviceCreateInfo deviceCI = {};
	deviceCI.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCI.queueCreateInfoCount = static_cast<uint32_t>(queueCIs.size());
	deviceCI.pQueueCreateInfos = queueCIs.data();
	deviceCI.enabledLayerCount = static_cast<uint32_t>(requiredValidationLayers.size());
	deviceCI.ppEnabledLayerNames = requiredValidationLayers.data();
	deviceCI.enabledExtensionCount = static_cast<uint32_t>(requiredDeviceExtensions.size());
	deviceCI.ppEnabledExtensionNames = requiredDeviceExtensions.data();
	deviceCI.pEnabledFeatures = &deviceFeatures;

	// Try to create the device
	if (vkCreateDevice(VKC::get().physicalDevice, &deviceCI, nullptr, &VKC::get().device) != VK_SUCCESS)
	{
		std::cerr << "Error: cannot create logical device." << std::endl;
		std::exit(-1);
	}

	// Once we have a device, we can get handles to each queue we requested
	// Doesn't matter if they repeat, we store them separately
	vkGetDeviceQueue(VKC::get().device, indices.graphicsFamily.value(), 0, &VKC::get().graphicsQueue);
	vkGetDeviceQueue(VKC::get().device, indices.presentFamily.value(), 0, &VKC::get().presentQueue);
	vkGetDeviceQueue(VKC::get().device, indices.transferFamily.value(), 0, &VKC::get().transferQueue);

	// Query the device properties to store the uniform buffer offset alignment
	//
	// This is necessary if we're using an uniform buffer with multiple components
	// and we want to map them to the buffer
	VkPhysicalDeviceProperties properties = {};
	vkGetPhysicalDeviceProperties(VKC::get().physicalDevice, &properties);
	VKC::get().minUniformBufferOffsetAlignment = properties.limits.minUniformBufferOffsetAlignment;
}


/**
 * @brief Creates a surface via GLFW
*/
void Application::createSurface()
{
	// Tries to create the surface
	if (glfwCreateWindowSurface(instance, VKC::get().window, nullptr, &surface) != VK_SUCCESS)
	{
		std::cerr << "Error: Failed to create window surface." << std::endl;
		std::exit(-1);
	}
}


/**
 * @brief Chooses a valid extent
 * @param capabilities
 * @return the calculated extent
*/
VkExtent2D Application::chooseExtent(const VkSurfaceCapabilitiesKHR& capabilities)
{
	// Given the capabilities of the surface, we can get the current extent
	VkExtent2D extent = capabilities.currentExtent;

	// If there's a problem with the width (i.e. maximum possible)
	if (capabilities.currentExtent.width == UINT32_MAX)
	{
		// Populate the width and height of the framebuffer
		glfwGetFramebufferSize(VKC::get().window, &VKC::get().windowWidth, &VKC::get().windowHeight);

		// Populate the old extent
		extent =
		{
			static_cast<uint32_t>(VKC::get().windowWidth),
			static_cast<uint32_t>(VKC::get().windowHeight)
		};

		// Getting the max possible extent which is in the limits of the window and the surface
		extent.width = std::max(capabilities.minImageExtent.width,
			std::min(capabilities.maxImageExtent.width, extent.width));
		extent.height = std::max(capabilities.minImageExtent.height,
			std::min(capabilities.maxImageExtent.height, extent.height));
	}

	return extent;
}


/**
 * @brief Method that creates the swapchain
*/
void Application::createSwapchain()
{
	// Query for the swapchain support details
	SwapchainSupportDetails swapchainSupportDetails = querySwapchainSupport(VKC::get().physicalDevice);

	// Storing the best surface format that will be used within the swapchain creation
	VkSurfaceFormatKHR surfaceFormat = Utility::chooseSurfaceFormat(swapchainSupportDetails.formats);

	// Choose the best present mode that will be used within the swapchain creation
	VkPresentModeKHR presentMode = Utility::choosePresentMode(swapchainSupportDetails.presentModes);

	// Choosing the correct extent for the swapchain framebuffers
	VkExtent2D extent = chooseExtent(swapchainSupportDetails.capabilities);

	// We want at least 2 images in the swapchain
	// so we get the min image count (which can be at least 1) and then we add 1
	uint32_t imageCount = swapchainSupportDetails.capabilities.minImageCount + 1;

	// Clamp the image count if needed
	// We don't want to have more images than the maximum allowed
	if (swapchainSupportDetails.capabilities.maxImageCount > 0 &&
		imageCount > swapchainSupportDetails.capabilities.maxImageCount)
	{
		imageCount = swapchainSupportDetails.capabilities.maxImageCount;
	}

	// Populate the swapchain create info struct
	// We assign the surface we created (via glfw)
	// We set the image count, format & color space, as well as the extent
	// We're only using 1 array layer
	VkSwapchainCreateInfoKHR swapchainCI = {};
	swapchainCI.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapchainCI.surface = surface;
	swapchainCI.minImageCount = imageCount;
	swapchainCI.imageFormat = surfaceFormat.format;
	swapchainCI.imageColorSpace = surfaceFormat.colorSpace;
	swapchainCI.imageExtent = extent;
	swapchainCI.imageArrayLayers = 1;
	swapchainCI.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	// Store the queue family indices
	QueueFamilyIndices indices = VKC::get().queueFamilies;

	std::set<uint32_t> queueFamilyIndices = { indices.graphicsFamily.value(), indices.presentFamily.value(), indices.transferFamily.value() };

	std::vector<uint32_t> queueFamilyIndicesVec = { queueFamilyIndices.cbegin(), queueFamilyIndices.cend() };

	// We decide on the behavior of the queues
	// If the indices differ, we want them to work concurrently
	// Otherwise, we want them to work in an exclusive manner
	if (queueFamilyIndicesVec.size() >= 2)
	{
		swapchainCI.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		swapchainCI.queueFamilyIndexCount = static_cast<uint32_t>(queueFamilyIndices.size());
		swapchainCI.pQueueFamilyIndices = queueFamilyIndicesVec.data();
	}
	else
	{
		swapchainCI.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	}


	swapchainCI.preTransform = swapchainSupportDetails.capabilities.currentTransform;
	// No alpha composition for the images in the swapchain
	swapchainCI.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

	// Set the present mode to be the chosen one
	swapchainCI.presentMode = presentMode;

	// Clipping is enabled so that we don't render outside the extent
	swapchainCI.clipped = VK_TRUE;

	// No old swapchain as this is the first one we create
	swapchainCI.oldSwapchain = VK_NULL_HANDLE;

	// Try to create a swapchain
	if (vkCreateSwapchainKHR(VKC::get().device, &swapchainCI, nullptr, &VKC::get().swapchain) != VK_SUCCESS)
	{
		std::cerr << "Error: Could not create swapchain." << std::endl;
		std::exit(-1);
	}

	// Query and store the images from the swapchain
	vkGetSwapchainImagesKHR(VKC::get().device, VKC::get().swapchain, &imageCount, nullptr);
	VKC::get().swapchainImages.resize(imageCount);
	vkGetSwapchainImagesKHR(VKC::get().device, VKC::get().swapchain, &imageCount, VKC::get().swapchainImages.data());

	// Store the image format and extent for later
	VKC::get().swapchainImageFormat = surfaceFormat.format;
	VKC::get().swapchainExtent = extent;
}


/**
 * @brief Create image views
*/
void Application::createImageViews()
{
	// Resizes the swapchain image views vector to the count of the swapchain images
	VKC::get().swapchainImageViews.resize(VKC::get().swapchainImages.size());

	// For each image, we create a corresponding image view
	for (size_t i = 0; i < VKC::get().swapchainImages.size(); i++)
	{
		Utility::createImageView(VKC::get().device, VKC::get().swapchainImageViews[i], VKC::get().swapchainImages[i], VK_IMAGE_VIEW_TYPE_2D, VKC::get().swapchainImageFormat, VK_IMAGE_ASPECT_COLOR_BIT, 1, 1);
	}
}


/**
 * @brief Creates a graphics pipeline
*/
void Application::createGraphicsPipeline()
{
	// We perform online shader compilation (as this is done once)
	// at the beginning of the application
	fs::path p = MAIN_DIR;
	p /= VKC::get().shadersPath;
	p /= VKC::get().currentShaderPath;

	std::string vsName = "vs.vert";
	std::string fsName = "fs.frag";

	fs::path vsPath = p / vsName;
	fs::path fsPath = p / fsName;

	// Perform the shader compilation via the shader compiler
	auto& shaderCompiler = VKC::get().shaderCompiler;

	// Within the src directory, we load the shader code
	// as a long string
	auto vsShaderCode = shaderCompiler.loadShader(vsPath.string());
	auto fsShaderCode = shaderCompiler.loadShader(fsPath.string());

	// Given the code, and where it is executing, we will compile the shader to SPIRV
	auto vsSPV = shaderCompiler.compileToSPIRV(vsName, shaderc_glsl_vertex_shader, vsShaderCode);
	auto fsSPV = shaderCompiler.compileToSPIRV(fsName, shaderc_glsl_fragment_shader, fsShaderCode);

	// Create the shader modules for both VS/FS
	VkShaderModule vsModule = Utility::createShaderModule(vsSPV);
	VkShaderModule fsModule = Utility::createShaderModule(fsSPV);

	// Prepare the shader stages create info structures
	// which indicate where each module is going to run
	std::vector<VkPipelineShaderStageCreateInfo> shaderStages;
	{
		VkPipelineShaderStageCreateInfo vsStageCI = {};
		vsStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		vsStageCI.stage = VK_SHADER_STAGE_VERTEX_BIT;
		vsStageCI.module = vsModule;
		vsStageCI.pName = "main";
		shaderStages.push_back(vsStageCI);

		VkPipelineShaderStageCreateInfo fsStageCI = {};
		fsStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		fsStageCI.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		fsStageCI.module = fsModule;
		fsStageCI.pName = "main";
		shaderStages.push_back(fsStageCI);
	}

	//////////////////////////////////////////////////
	// Vertex input stage ----------------------------

	// Prepare the general binding description
	// This indicates the size of each input and how we send it (e.g. as a vertex)
	VkVertexInputBindingDescription vertexInputBindingDescription =
		Vertex::getBindingDescription();

	// Prepare the attribute description
	// For each input, we need to specify its size and type
	std::vector<VkVertexInputAttributeDescription> vertexAttributesDescriptions =
		Vertex::getAttributeDescriptions();

	// Populate the vertex input state CI struct with the information gathered
	VkPipelineVertexInputStateCreateInfo vertexInputCI = {};
	vertexInputCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputCI.vertexBindingDescriptionCount = 1;
	vertexInputCI.pVertexBindingDescriptions = &vertexInputBindingDescription;
	vertexInputCI.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertexAttributesDescriptions.size());
	vertexInputCI.pVertexAttributeDescriptions = vertexAttributesDescriptions.data();

	////////////////////////////////////////////////////
	// Input assembly stage ----------------------------
	VkPipelineInputAssemblyStateCreateInfo inputAssemblyCI = {};
	inputAssemblyCI.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssemblyCI.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssemblyCI.primitiveRestartEnable = VK_FALSE;

	////////////////////////////////////////////////////
	// No Tessellation ---------------------------- SKIPPED

	///////////////////////////////////////////////////
	// Viewport state ----------------------------
	VkPipelineViewportStateCreateInfo viewportCI = {};
	viewportCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportCI.viewportCount = 1;
	// This is nullptr (even though count is 1) because we have a dynamic viewport
	viewportCI.pViewports = nullptr;
	viewportCI.scissorCount = 1;
	// This is nullptr (even though count is 1) because we have a dynamic scissor
	viewportCI.pScissors = nullptr;

	///////////////////////////////////////////////////
	// Rasterization State -------------------------
	// Here we set the rasterization stage settings
	//
	// Mainly, we don't want depth clamp and discard
	// We set the culling to only apply for the back-facing triangles
	// and we set the front face to be interpreted as clockwise
	// We also do polygon fills so we have full triangles displayed
	VkPipelineRasterizationStateCreateInfo rasterizationCI = {};
	rasterizationCI.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizationCI.depthClampEnable = VK_FALSE;
	rasterizationCI.rasterizerDiscardEnable = VK_FALSE;
	rasterizationCI.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizationCI.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizationCI.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterizationCI.depthBiasEnable = VK_FALSE;
	rasterizationCI.lineWidth = 1.0f;

	//////////////////////////////////////////////////
	// Multi-sample State ------------------------
	// Here we set the multi sampling samples (which I set a max of 4
	// if the device has the possibility to do so)
	VkPipelineMultisampleStateCreateInfo multisampleCI = {};
	multisampleCI.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampleCI.rasterizationSamples = VKC::get().msaaSamples;
	multisampleCI.sampleShadingEnable = VK_FALSE;

	/////////////////////////////////////////////////
	// Depth Stencil State -----------------------
	// Here we tell the pipeline to perform depth testing
	// and to write the result where [0 = close, 1 = far]
	VkPipelineDepthStencilStateCreateInfo depthStencilCI = {};
	depthStencilCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthStencilCI.depthTestEnable = VK_TRUE;
	depthStencilCI.depthWriteEnable = VK_TRUE;
	depthStencilCI.depthCompareOp = VK_COMPARE_OP_LESS;
	depthStencilCI.depthBoundsTestEnable = VK_FALSE;
	depthStencilCI.minDepthBounds = 0.0f;
	depthStencilCI.maxDepthBounds = 1.0f;
	depthStencilCI.stencilTestEnable = VK_FALSE;
	depthStencilCI.front = {};
	depthStencilCI.back = {};

	// Color attachment
	// We perform src one, dst one for color blending
	// Using the created color attachment in the color blend
	VkPipelineColorBlendAttachmentState colorAttachment = {};
	colorAttachment.blendEnable = VK_TRUE;
	colorAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
	colorAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorAttachment.colorBlendOp = VK_BLEND_OP_ADD;
	colorAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	colorAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

	////////////////////////////////////////////////
	// Color Blend State -------------------------
	// Here we specify if we enable the color blending and what
	// the operation is
	//
	VkPipelineColorBlendStateCreateInfo colorBlendCI = {};
	colorBlendCI.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlendCI.logicOpEnable = VK_FALSE;
	colorBlendCI.logicOp = VK_LOGIC_OP_COPY;
	colorBlendCI.attachmentCount = 1;
	colorBlendCI.pAttachments = &colorAttachment;
	// No blending constants
	colorBlendCI.blendConstants[0] = 0.0f;
	colorBlendCI.blendConstants[1] = 0.0f;
	colorBlendCI.blendConstants[2] = 0.0f;
	colorBlendCI.blendConstants[3] = 0.0f;

	// Dynamic State -----------------------------
	// We will use the viewport and scissor as dynamic states
	std::vector<VkDynamicState> dynamicStates =
	{
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_SCISSOR
	};

	// Populating the pipeline dynamic state with our dynamic states
	VkPipelineDynamicStateCreateInfo dynamicCI = {};
	dynamicCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicCI.dynamicStateCount = static_cast<uint32_t>(dynamicStates.size());
	dynamicCI.pDynamicStates = dynamicStates.data();

	// Our pipeline layout consists for the following descriptor sets
	// in that specific order
	// This is because we bind the descriptor sets by frequency of their updates
	std::vector<VkDescriptorSetLayout> layouts =
	{
		camera.descriptorSetLayout,
		light.descriptorSetLayout,
		duckActor.descriptorSetLayout
	};

	// Creating a vector of possible push constants
	std::vector<VkPushConstantRange> pushConstantRanges;
	{
		// Creating the push constant range for the rendering toggle-able features
		// that will only be available in the fragment shader
		VkPushConstantRange pcRange = {};
		pcRange.offset = 0;
		pcRange.size = sizeof(PushConstantShaderFeatures);
		pcRange.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
		pushConstantRanges.push_back(pcRange);
	}

	// Creating the pipeline layout
	// This includes the descriptor set layouts
	// and the push constants
	VkPipelineLayoutCreateInfo pipelineLayoutCI = {};
	pipelineLayoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCI.setLayoutCount = static_cast<uint32_t>(layouts.size());
	pipelineLayoutCI.pSetLayouts = layouts.data();
	pipelineLayoutCI.pushConstantRangeCount = static_cast<uint32_t>(pushConstantRanges.size());
	pipelineLayoutCI.pPushConstantRanges = pushConstantRanges.data();

	// Try to create the layout
	if (vkCreatePipelineLayout(VKC::get().device, &pipelineLayoutCI, nullptr, &graphicsPipelineLayout) != VK_SUCCESS)
	{
		std::cerr << "Error: Cannot create pipeline layout." << std::endl;
		std::exit(-1);
	}

	// Populate the graphics pipeline with the data accumulated above
	VkGraphicsPipelineCreateInfo pipelineCI = {};
	pipelineCI.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineCI.stageCount = static_cast<uint32_t>(shaderStages.size());
	pipelineCI.pStages = shaderStages.data();
	pipelineCI.pVertexInputState = &vertexInputCI;
	pipelineCI.pInputAssemblyState = &inputAssemblyCI;
	pipelineCI.pTessellationState = nullptr;
	pipelineCI.pViewportState = &viewportCI;
	pipelineCI.pRasterizationState = &rasterizationCI;
	pipelineCI.pMultisampleState = &multisampleCI;
	pipelineCI.pDepthStencilState = &depthStencilCI;
	pipelineCI.pColorBlendState = &colorBlendCI;
	pipelineCI.pDynamicState = &dynamicCI;
	pipelineCI.layout = graphicsPipelineLayout;
	pipelineCI.renderPass = renderPass;
	pipelineCI.subpass = 0;
	pipelineCI.basePipelineHandle = VK_NULL_HANDLE;
	pipelineCI.basePipelineIndex = -1;

	// Creating the graphics
	if (vkCreateGraphicsPipelines(VKC::get().device, VK_NULL_HANDLE, 1, &pipelineCI, nullptr, &graphicsPipeline) != VK_NULL_HANDLE)
	{
		std::cerr << "Error: Failed to create graphics pipeline." << std::endl;
		std::exit(-1);
	}

	// Clear shader modules as not needed anymore
	vkDestroyShaderModule(VKC::get().device, vsModule, nullptr);
	vkDestroyShaderModule(VKC::get().device, fsModule, nullptr);
}


/**
 * @brief Creates the renderpass used
*/
void Application::createRenderpass()
{
	// Creating the attachments for this renderpass

	// Creating the color attachment that will have multi sampling
	// We clear it on load, and we store it for later
	// This will be resolved in the third attachment
	VkAttachmentDescription colorAttachment = {};
	colorAttachment.format = VKC::get().swapchainImageFormat;
	colorAttachment.samples = VKC::get().msaaSamples;
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	// Not keeping stencil
	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	// Creating the depth attachment, same number of samples
	// We clear it on load, and we don't use it afterwards
	VkAttachmentDescription depthAttachment = {};
	depthAttachment.format = Utility::findDepthFormat(VKC::get().physicalDevice);
	depthAttachment.samples = VKC::get().msaaSamples;
	depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	// Resolving multi-sampling attachment
	// Accumulates the color attachment, then resolves it here
	VkAttachmentDescription colorAttachmentResolve = {};
	colorAttachmentResolve.format = VKC::get().swapchainImageFormat;
	colorAttachmentResolve.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachmentResolve.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachmentResolve.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachmentResolve.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachmentResolve.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachmentResolve.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachmentResolve.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	// Subpass color attachment reference
	VkAttachmentReference colorAttachmentReference = {};
	colorAttachmentReference.attachment = 0;
	colorAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	// Depth attachment reference
	VkAttachmentReference depthAttachmentReference = {};
	depthAttachmentReference.attachment = 1;
	depthAttachmentReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	// Subpass resolve color attachment reference
	VkAttachmentReference colorResolveAttachmentReference = {};
	colorResolveAttachmentReference.attachment = 2;
	colorResolveAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	// Subpass description
	// We're going to have only one subpass
	// containing the attachments created above
	VkSubpassDescription subpassDescription = {};
	subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpassDescription.colorAttachmentCount = 1;
	subpassDescription.pColorAttachments = &colorAttachmentReference;
	subpassDescription.pDepthStencilAttachment = &depthAttachmentReference;
	subpassDescription.pResolveAttachments = &colorResolveAttachmentReference;

	// Creating 2 dependencies
	// The first one indicates that we wait for an external incoming subpass
	//
	// We are performing all the work until the COLOR ATTACHMENT OUTPUT
	// Then we are waiting for the input subpass to arrive at the bottom of the pipe
	// This is just like a barrier
	std::vector<VkSubpassDependency> dependencies(2);
	dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
	dependencies[0].dstSubpass = 0;
	dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	// The second one indicates that we're waiting for the subpass 0 to finish
	// and our target is the external next subpass.
	// Same as above, we let the next subpass work completely
	dependencies[1].srcSubpass = 0;
	dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
	dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	// The order matters, so we created this vector
	// of attachment descriptions
	std::vector<VkAttachmentDescription> attachmentReferences =
	{
		colorAttachment, depthAttachment, colorAttachmentResolve
	};

	// Renderpass info
	// We're using the attachments created before,
	// As well as the subpass and its dependencies
	VkRenderPassCreateInfo renderPassCI = {};
	renderPassCI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassCI.attachmentCount = static_cast<uint32_t>(attachmentReferences.size());
	renderPassCI.pAttachments = attachmentReferences.data();
	renderPassCI.subpassCount = 1;
	renderPassCI.pSubpasses = &subpassDescription;
	renderPassCI.dependencyCount = static_cast<uint32_t>(dependencies.size());
	renderPassCI.pDependencies = dependencies.data();

	// Try to create a renderpass
	if (vkCreateRenderPass(VKC::get().device, &renderPassCI, nullptr, &renderPass) != VK_SUCCESS)
	{
		std::cerr << "Error: Could not create the renderpass." << std::endl;
		std::exit(-1);
	}
}


/**
 * @brief Creates a framebuffer for each swapchain image
*/
void Application::createFramebuffers()
{
	// We need to have as many framebuffers as image views
	swapchainFramebuffers.resize(VKC::get().swapchainImageViews.size());

	for (size_t i = 0; i < swapchainFramebuffers.size(); i++)
	{
		// Each framebuffer needs to write into a specific attachment
		std::vector<VkImageView> attachments =
		{
			colorImageView, depthImageView, VKC::get().swapchainImageViews[i]
		};

		// The framebuffer is bound to the renderpass created earlier
		VkFramebufferCreateInfo framebufferCI = {};
		framebufferCI.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferCI.renderPass = renderPass;
		framebufferCI.attachmentCount = static_cast<uint32_t>(attachments.size());
		framebufferCI.pAttachments = attachments.data();
		framebufferCI.width = VKC::get().swapchainExtent.width;
		framebufferCI.height = VKC::get().swapchainExtent.height;
		framebufferCI.layers = 1;

		// Try to create a framebuffer
		if (vkCreateFramebuffer(VKC::get().device, &framebufferCI, nullptr, &swapchainFramebuffers[i]) != VK_SUCCESS)
		{
			std::cerr << "Error: Cannot create framebuffer id " << i << "." << std::endl;
			std::exit(-1);
		}
	}
}


/**
 * @brief Creates a command pool
*/
void Application::createCommandPool()
{
	// Getting the queue families
	QueueFamilyIndices queueFamilyIndices = VKC::get().queueFamilies;

	// Command pool for the graphics queue
	// We are allowed to reset it, as well as the command buffers
	VkCommandPoolCreateInfo cmdPoolCI = {};
	cmdPoolCI.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	cmdPoolCI.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value();
	cmdPoolCI.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	// Try to create a command pool
	if (vkCreateCommandPool(VKC::get().device, &cmdPoolCI, nullptr, &VKC::get().commandPool) != VK_SUCCESS)
	{
		std::cerr << "Error: Could not create command pool." << std::endl;
		std::exit(-1);
	}
}


/**
 * @brief Begins a renderpass
*/
void Application::createCommandBuffers()
{
	// We need one command buffer for each framebuffer
	commandBuffers.resize(swapchainFramebuffers.size());

	// Fill in an allocate info structure for the command buffer
	// We will get our command buffers from the general command pool
	// All of the command buffers will be primary
	VkCommandBufferAllocateInfo cmdBufAI = {};
	cmdBufAI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	cmdBufAI.commandPool = VKC::get().commandPool;
	cmdBufAI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	cmdBufAI.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());

	// Try to allocate them
	if (vkAllocateCommandBuffers(VKC::get().device, &cmdBufAI, commandBuffers.data()) != VK_SUCCESS)
	{
		std::cerr << "Error: Could not allocate command buffers." << std::endl;
		std::exit(-1);
	}

	// For each frame, we will have a recording to be made
	for (size_t i = 0; i < commandBuffers.size(); i++)
	{
		VkCommandBufferBeginInfo cmdBufBI = {};
		cmdBufBI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

		// Try to start recording the command buffer
		if (vkBeginCommandBuffer(commandBuffers[i], &cmdBufBI) != VK_SUCCESS)
		{
			std::cerr << "Error: Could not begin recording of command buffer." << std::endl;
			std::exit(-1);
		}

		// Fill in the clear values for color & depth/stencil
		VkClearValue clearColor = { 0.0f, 0.0f, 0.0f, 1.0f };
		VkClearValue depthStencil = { 1.0f, 0.0f };
		std::vector<VkClearValue> clearValues = { clearColor, depthStencil };

		// Create a render pass begin info structure
		// pointing to the current frame's framebuffer
		VkRenderPassBeginInfo renderPassBI = {};
		renderPassBI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassBI.renderPass = renderPass;
		renderPassBI.framebuffer = swapchainFramebuffers[i];
		renderPassBI.renderArea.offset = { 0, 0 };
		renderPassBI.renderArea.extent = VKC::get().swapchainExtent;
		renderPassBI.clearValueCount = static_cast<uint32_t>(clearValues.size());
		renderPassBI.pClearValues = clearValues.data();

		// Create a dynamic viewport which will be bounded in the render pass
		VkViewport viewport = {};
		viewport.x = 0.0f;
		viewport.y = static_cast<float>(VKC::get().swapchainExtent.height);
		viewport.width = static_cast<float>(VKC::get().swapchainExtent.width);
		viewport.height = -static_cast<float>(VKC::get().swapchainExtent.height);
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

		// Create a dynamic scissor which will be bounded to the viewport in the render pass
		VkRect2D scissor = {};
		scissor.offset = { 0, 0 };
		scissor.extent = VKC::get().swapchainExtent;

		// Begin the renderpass
		// where first we draw the geometry, then we draw the UI overlay
		vkCmdBeginRenderPass(commandBuffers[i], &renderPassBI, VK_SUBPASS_CONTENTS_INLINE);
		{
			// Drawing geometry
			{
				// Binding the graphics pipeline for the geometry
				vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);
				// Binding the viewport & scissor as they're dynamic
				vkCmdSetViewport(commandBuffers[i], 0, 1, &viewport);
				vkCmdSetScissor(commandBuffers[i], 0, 1, &scissor);

				// We're not changing those descriptor sets at all
				// The only one bound continuously (i.e. more frequent) is the actor descriptor set
				std::vector<VkDescriptorSet> fixedDescriptorSets =
				{
					camera.descriptorSets[i], light.descriptorSets[i]
				};

				// Hence, it is faster to perform less bindings overall
				// and we bind them by frequency (ascending)
				//
				vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipelineLayout, 0, static_cast<uint32_t>(fixedDescriptorSets.size()), fixedDescriptorSets.data(), 0, nullptr);

				// Push general constants (no need for binding per actor)
				vkCmdPushConstants(commandBuffers[i], graphicsPipelineLayout, VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(PushConstantShaderFeatures), &pushConstants);

				// Usually, here, we draw all the scene.
				// If we were to have manual culling & clustering, we will perform a
				// single for-loop through the already-sorted list of visible actors
				// and enqueue them to rendering
				VkDeviceSize offsets = 0;
				{
					// Hence, the most frequent descriptor set is the
					// actor descriptor set
					std::vector<VkDescriptorSet> frequentDescriptorSets =
					{
						duckActor.descriptorSets[i]
					};

					// Binding the actor's index & vertex buffer
					vkCmdBindIndexBuffer(commandBuffers[i], duckActor.mesh.indexBuffer.buffer, offsets, VK_INDEX_TYPE_UINT32);
					vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, &duckActor.mesh.vertexBuffer.buffer, &offsets);

					// As well as its descriptor set
					// but now we're using an offset of the size of the fixed descriptor sets
					vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipelineLayout, static_cast<uint32_t>(fixedDescriptorSets.size()), static_cast<uint32_t>(frequentDescriptorSets.size()), frequentDescriptorSets.data(), 0, nullptr);

					// Draw the mesh using the index buffer
					vkCmdDrawIndexed(commandBuffers[i], static_cast<uint32_t>(duckActor.mesh.indices.size()), 1, 0, 0, 0);
				}
			}

			// Drawing the UI
			uiOverlay.draw(commandBuffers[i], static_cast<uint32_t>(i), swapchainFramebuffers[i]);
		}
		// End recording the renderpass
		vkCmdEndRenderPass(commandBuffers[i]);

		// Try end the command buffer
		if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS)
		{
			std::cerr << "Error: Could not end the recording of the command buffer." << std::endl;
			std::exit(-1);
		}
	}
}


/**
 * @brief Create sync objects such as fences, semaphores, etc.
*/
void Application::createSyncObjects()
{
	// Create a general semaphore create info
	VkSemaphoreCreateInfo semaphoreCI = {};
	semaphoreCI.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	// Create a general signaled fence create info
	VkFenceCreateInfo fenceCI = {};
	fenceCI.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceCI.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	// We resize those by the number of frames we have in flight
	imageAvailableSemaphores.resize(maxFramesInFlight);
	renderFinishedSemaphores.resize(maxFramesInFlight);
	inFlightFences.resize(maxFramesInFlight);
	fenceImagesInFlight.resize(VKC::get().swapchainImages.size(), VK_NULL_HANDLE);

	// For each frame, we create the corresponding semaphores, fence
	for (size_t i = 0; i < maxFramesInFlight; i++)
	{
		if (vkCreateSemaphore(VKC::get().device, &semaphoreCI, nullptr, &imageAvailableSemaphores[i]) != VK_SUCCESS)
		{
			std::cerr << "Error: Could not create image semaphore." << std::endl;
			std::exit(-1);
		}

		if (vkCreateSemaphore(VKC::get().device, &semaphoreCI, nullptr, &renderFinishedSemaphores[i]) != VK_SUCCESS)
		{
			std::cerr << "Error: Could not create render semaphore." << std::endl;
			std::exit(-1);
		}

		if (vkCreateFence(VKC::get().device, &fenceCI, nullptr, &inFlightFences[i]) != VK_SUCCESS)
		{
			std::cerr << "Error: Could not create fence." << std::endl;
			std::exit(-1);
		}
	}
}


/**
 * @brief Cleans the old swapchain
 * Mainly destroying anything related to the swapchain
*/
void Application::cleanupOldSwapchain()
{
	// Destroy the color image view and its associated img & memory
	vkDestroyImageView(VKC::get().device, colorImageView, nullptr);
	vkDestroyImage(VKC::get().device, colorImage, nullptr);
	vkFreeMemory(VKC::get().device, colorImageMemory, nullptr);

	// Destroy the depth image view and its associated img & memory
	vkDestroyImageView(VKC::get().device, depthImageView, nullptr);
	vkDestroyImage(VKC::get().device, depthImage, nullptr);
	vkFreeMemory(VKC::get().device, depthImageMemory, nullptr);

	// Destroys the framebuffers
	for (auto& framebuffer : swapchainFramebuffers)
	{
		vkDestroyFramebuffer(VKC::get().device, framebuffer, nullptr);
	}
	// Frees command buffers
	vkFreeCommandBuffers(VKC::get().device, VKC::get().commandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());
	// Destroy renderpass
	vkDestroyRenderPass(VKC::get().device, renderPass, nullptr);
	// Destroys all swapchain image views
	for (auto& imageView : VKC::get().swapchainImageViews)
	{
		vkDestroyImageView(VKC::get().device, imageView, nullptr);
	}
	// Destroys swapchain
	vkDestroySwapchainKHR(VKC::get().device, VKC::get().swapchain, nullptr);

	// Cleanup uniform buffers
	for (auto& uniformBuffer : VKC::get().uniformBuffers)
	{
		uniformBuffer.destroy();
	}

	// Destroys the descriptor pool
	vkDestroyDescriptorPool(VKC::get().device, VKC::get().descriptorPool, nullptr);
}


/**
 * @brief Recreates the swapchain, and whatever was destroyed in
 * the method @cleanupOldSwapchain
*/
void Application::recreateSwapchain()
{
	// As long as we don't have a valid screen size
	int width = 0;
	int height = 0;
	do
	{
		glfwGetFramebufferSize(VKC::get().window, &width, &height);
		glfwWaitEvents();
	} while (width == 0 || height == 0);

	// Wait for the device
	vkDeviceWaitIdle(VKC::get().device);

	// Cleans up the old swapchain
	cleanupOldSwapchain();

	// Perform a re-allocation of swapchain images, image views
	// recreating the renderpass, resources, framebuffers, descriptor pool
	// and updating the user data descriptor sets (as they belong to the general one)
	// recreates the command buffers as well as the size of the swapchain changes
	createSwapchain();
	createImageViews();
	createRenderpass();
	createColorResources();
	createDepthResources();
	createFramebuffers();
	createUniformBuffers();
	createDescriptorPool();
	generateUserDataDSL(true);
	createCommandBuffers();
}


/**
 * @brief Draw method for submitting the work to the GPU
*/
void Application::draw()
{
	uint32_t imageID = 0;

	// We first wait for any fence if there's anything in flight
	vkWaitForFences(VKC::get().device, 1, &inFlightFences[currentFrame], VK_TRUE, UINT64_MAX);

	// We acquire the next image to be written from the swapchain and store its id
	VkResult result = vkAcquireNextImageKHR(VKC::get().device, VKC::get().swapchain, UINT64_MAX, imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, &imageID);

	// If the swapchain needs to be updated, we update it
	if (result == VK_ERROR_OUT_OF_DATE_KHR)
	{
		recreateSwapchain();
		return;
	}
	// Otherwise, if not success or it is suboptimal, an error happened
	else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
	{
		std::cerr << "Error: Failed to acquire next image." << std::endl;
		std::exit(-1);
	}

	// If the new image is in flight, we wait for it
	if (fenceImagesInFlight[imageID] != VK_NULL_HANDLE)
	{
		vkWaitForFences(VKC::get().device, 1, &fenceImagesInFlight[imageID], VK_TRUE, UINT64_MAX);
	}

	// We set the fence image in flight to be the one above
	fenceImagesInFlight[imageID] = inFlightFences[currentFrame];

	// Recreate the command buffer for the imageID
	// as well as its uniform buffer
	updateCommandBuffer(imageID);
	updateUniformBuffer(imageID);

	// We populate a vector of semaphores we need to wait for
	std::vector<VkSemaphore> waitSemaphores =
	{
		imageAvailableSemaphores[currentFrame]
	};

	// We mainly need to wait for the color attachment to be written
	std::vector<VkPipelineStageFlags> waitStages =
	{
		VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
	};

	// We will need to signal the following semaphore upon rendering
	std::vector<VkSemaphore> signalSemaphores =
	{
		renderFinishedSemaphores[currentFrame]
	};

	// Populate a submit info
	// waiting for the specified semaphores
	// then we submit the command buffers with the image id
	//
	// the submit will signal the semaphores specified
	VkSubmitInfo submitI = {};
	submitI.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitI.waitSemaphoreCount = static_cast<uint32_t>(waitSemaphores.size());
	submitI.pWaitSemaphores = waitSemaphores.data();
	submitI.pWaitDstStageMask = waitStages.data();
	submitI.commandBufferCount = 1;
	submitI.pCommandBuffers = &commandBuffers[imageID];
	submitI.signalSemaphoreCount = static_cast<uint32_t>(signalSemaphores.size());
	submitI.pSignalSemaphores = signalSemaphores.data();

	vkResetFences(VKC::get().device, 1, &inFlightFences[currentFrame]);

	// submit the queue for the image id
	if (vkQueueSubmit(VKC::get().graphicsQueue, 1, &submitI, inFlightFences[currentFrame]) != VK_SUCCESS)
	{
		std::cerr << "Error: Could not submit graphics queue for work at frame-id " << imageID << "." << std::endl;
		std::exit(-1);
	}

	std::vector<VkSwapchainKHR> swapchains =
	{
		VKC::get().swapchain
	};

	// Fill in a present info that will
	// wait for the signal semaphores to be signaled
	// and will present the image
	VkPresentInfoKHR presentI = {};
	presentI.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentI.waitSemaphoreCount = static_cast<uint32_t>(signalSemaphores.size());
	presentI.pWaitSemaphores = signalSemaphores.data();
	presentI.swapchainCount = static_cast<uint32_t>(swapchains.size());
	presentI.pSwapchains = swapchains.data();
	presentI.pImageIndices = &imageID;

	// Present the queue
	result = vkQueuePresentKHR(VKC::get().presentQueue, &presentI);

	// If we need to recreate the swapchain
	if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || framebufferResized)
	{
		framebufferResized = false;
		recreateSwapchain();
		return;
	}
	// If error
	else if (result != VK_SUCCESS)
	{
		std::cerr << "Error: Failed to present next image." << std::endl;
		std::exit(-1);
	}

	// Set the current frame id
	currentFrame = (currentFrame + 1) % maxFramesInFlight;
}


/**
 * @brief Create uniform buffers
*/
void Application::createUniformBuffers()
{
	// 100 structs of 256 bytes each
	// e.g. a 4x4matrix has 64 bytes, so this will allow 4 matrices (or equivalent) to be within
	VkDeviceSize bufferSize = 256 * 100;
	VKC::get().uniformBuffers.resize(VKC::get().swapchainImages.size());

	// For each frame, we will have a uniform buffer
	// of 25600 bytes
	for (size_t i = 0; i < VKC::get().swapchainImages.size(); i++)
	{
		VKC::get().uniformBuffers[i].create(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
		VKC::get().uniformBuffers[i].bind();
	}
}


/**
 * @brief Updates the uniform buffer for a specific frame
 * @param imageID
*/
void Application::updateUniformBuffer(const uint32_t& imageID)
{
	// Simply maps then populates the uniform buffer
	// We make use of the offsets that we allocated the components with
	auto& buffer = VKC::get().uniformBuffers[imageID];
	buffer.map(VK_WHOLE_SIZE);
	buffer.populateWith(&camera.transform, sizeof(Transform), camera.transform.offsetIntoBuffer);
	buffer.populateWith(&camera.projection, sizeof(glm::mat4), camera.projectionOffsetIntoBuffer);
	buffer.populateWith(&light.transform, sizeof(Transform), light.transform.offsetIntoBuffer);
	buffer.populateWith(&duckActor.transform, sizeof(Transform), duckActor.transform.offsetIntoBuffer);
	buffer.populateWith(&duckActor.mesh.material, sizeof(Material), duckActor.mesh.material.offsetIntoBuffer);
	buffer.unmap();
}


/**
 * @brief Create the general descriptor pool
*/
void Application::createDescriptorPool()
{
	// We will need at most 100 image samplers, and 100 uniform buffers
	std::vector<VkDescriptorPoolSize> poolSizes =
	{
		{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, static_cast<uint32_t>(100) },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, static_cast <uint32_t>(100) }
	};

	// Filling the descriptor pool create info
	// we can have at most 1000 sets (most of them are unused anyway)
	VkDescriptorPoolCreateInfo poolCI = {};
	poolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolCI.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
	poolCI.maxSets = 1000;
	poolCI.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
	poolCI.pPoolSizes = poolSizes.data();

	// Try to create the descriptor pool
	if (vkCreateDescriptorPool(VKC::get().device, &poolCI, nullptr, &VKC::get().descriptorPool) != VK_SUCCESS)
	{
		std::cerr << "Error: could not create descriptor pool." << std::endl;
		std::exit(-1);
	}
}


/**
 * @brief Create color resources
*/
void Application::createColorResources()
{
	// Get the available swapchain format
	VkFormat colorFormat = VKC::get().swapchainImageFormat;

	// Create the colorImage
	Utility::createImage(VKC::get().physicalDevice, VKC::get().device, colorImage, colorImageMemory, VKC::get().swapchainExtent.width, VKC::get().swapchainExtent.height, 1, 1, 1, VKC::get().msaaSamples, colorFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	// Create the colorImageView based on colorImage
	Utility::createImageView(VKC::get().device, colorImageView, colorImage, VK_IMAGE_VIEW_TYPE_2D, colorFormat, VK_IMAGE_ASPECT_COLOR_BIT, 1, 1);
}


/**
 * @brief Create the depth resources
*/
void Application::createDepthResources()
{
	// Get the good depth format available
	VkFormat depthFormat = Utility::findDepthFormat(VKC::get().physicalDevice);

	// Create the depth image and allocate its memory
	Utility::createImage(VKC::get().physicalDevice, VKC::get().device, depthImage, depthImageMemory, VKC::get().swapchainExtent.width, VKC::get().swapchainExtent.height, 1, 1, 1, VKC::get().msaaSamples, depthFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	// Create the depth image view based on the depth image
	Utility::createImageView(VKC::get().device, depthImageView, depthImage, VK_IMAGE_VIEW_TYPE_2D, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT, 1, 1);
}


/**
 * @brief Loads the duck model
*/
void Application::loadModel()
{
	// File-system augmentation
	fs::path modelPath = MAIN_DIR;
	modelPath /= VKC::get().modelsPath;
	modelPath /= VKC::get().objectName;

	// Loads the obj
	duckMesh = Mesh::loadObj(modelPath.generic_string());
	// then generates the buffers for the mesh
	duckActor.mesh = duckMesh[0];
	duckActor.mesh.generateBuffers();

	// Loads the texture
	fs::path p = MAIN_DIR;
	fs::path textureToLoadPath = p / VKC::get().texturesPath / "duck/duck.jpg";
	duckActor.texture.load(textureToLoadPath.string());
}


/**
 * @brief Maps the user resources (mesh, transform, etc)
 * to the uniform buffers created
*/
void Application::mapUserDataToBuffers()
{
	auto& ubuffers = VKC::get().uniformBuffers;

	// As long as there's a uniform buffer
	// we map each element in the buffer
	// calculating the offset via the min uniform buffer device alignment
	if (ubuffers.size() > 1)
	{
		// Camera elements
		camera.transform.offsetIntoBuffer = ubuffers[0].addElementToBuffer(sizeof(Transform));
		camera.projectionOffsetIntoBuffer = ubuffers[0].addElementToBuffer(sizeof(glm::mat4));

		// Light elements
		light.transform.offsetIntoBuffer = ubuffers[0].addElementToBuffer(sizeof(Transform));

		// Actor stuff elements
		duckActor.transform.offsetIntoBuffer = ubuffers[0].addElementToBuffer(sizeof(Transform));
		duckActor.mesh.material.offsetIntoBuffer = ubuffers[0].addElementToBuffer(sizeof(Material));

		// Ensure that the array of offsets is the same in both buffers
		for (size_t i = 1; i < ubuffers.size(); i++)
		{
			ubuffers[i].offsets = ubuffers[0].offsets;
		}
	}
}


/**
 * @brief [Re-]Generate the descriptor set layouts
 * @param reset
*/
void Application::generateUserDataDSL(const bool& reset)
{
	duckActor.generateDSL(reset);
	camera.generateDSL(reset);
	light.generateDSL(reset);
}


/**
 * @brief Destroys the data created
*/
void Application::destroyUserData()
{
	light.destroy();
	camera.destroy();
	duckActor.destroy();
}


/**
 * @brief Update the command buffer for a specific frame
 * @param id
*/
void Application::updateCommandBuffer(const uint32_t& id)
{
	// We reset if first
	vkResetCommandBuffer(commandBuffers[id], 0);

	// Command buffer begin info
	VkCommandBufferBeginInfo cmdBufBI = {};
	cmdBufBI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

	// We begin the reset command buffer
	if (vkBeginCommandBuffer(commandBuffers[id], &cmdBufBI) != VK_SUCCESS)
	{
		std::cerr << "Error: Could not begin recording of command buffer." << std::endl;
		std::exit(-1);
	}

	// Clear color and depth values
	VkClearValue clearColor = { 0.0f, 0.0f, 0.0f, 1.0f };
	VkClearValue depthStencil = { 1.0f, 0.0f };
	std::vector<VkClearValue> clearValues =
	{
		clearColor, depthStencil
	};

	// Create the render pass begin info
	// using the id to get the framebuffer
	VkRenderPassBeginInfo renderPassBI = {};
	renderPassBI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassBI.renderPass = renderPass;
	renderPassBI.framebuffer = swapchainFramebuffers[id];
	renderPassBI.renderArea.offset = { 0, 0 };
	renderPassBI.renderArea.extent = VKC::get().swapchainExtent;
	renderPassBI.clearValueCount = static_cast<uint32_t>(clearValues.size());
	renderPassBI.pClearValues = clearValues.data();

	// Create a dynamic viewport
	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = static_cast<float>(VKC::get().swapchainExtent.width);
	viewport.height = static_cast<float>(VKC::get().swapchainExtent.height);
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	// Create a dynamic scissor
	VkRect2D scissor = {};
	scissor.offset = { 0, 0 };
	scissor.extent = VKC::get().swapchainExtent;

	// Begin the renderpass
	// first, we draw the geometry
	// then we draw the UI
	vkCmdBeginRenderPass(commandBuffers[id], &renderPassBI, VK_SUBPASS_CONTENTS_INLINE);
	{
		// Get the ui overlay new frame and update its buffers
		uiOverlay.newFrame(*this);

		// Drawing geometry
		{
			// Bind the pipeline and the dynamic states
			vkCmdBindPipeline(commandBuffers[id], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);
			vkCmdSetViewport(commandBuffers[id], 0, 1, &viewport);
			vkCmdSetScissor(commandBuffers[id], 0, 1, &scissor);

			// Bind descriptor sets by frequency
			std::vector<VkDescriptorSet> fixedDescriptorSets =
			{
				camera.descriptorSets[id], light.descriptorSets[id]
			};
			vkCmdBindDescriptorSets(commandBuffers[id], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipelineLayout, 0, static_cast<uint32_t>(fixedDescriptorSets.size()), fixedDescriptorSets.data(), 0, nullptr);

			// Push constants once
			vkCmdPushConstants(commandBuffers[id], graphicsPipelineLayout, VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(PushConstantShaderFeatures), &pushConstants);

			// For each drawable mesh
			VkDeviceSize offsets = 0;
			{
				// Bind index & vertex buffers
				vkCmdBindIndexBuffer(commandBuffers[id], duckActor.mesh.indexBuffer.buffer, offsets, VK_INDEX_TYPE_UINT32);
				vkCmdBindVertexBuffers(commandBuffers[id], 0, 1, &duckActor.mesh.vertexBuffer.buffer, &offsets);

				// List of frequent descriptor sets
				std::vector<VkDescriptorSet> frequentDescriptorSets =
				{
					duckActor.descriptorSets[id]
				};
				// Bind frequent descriptor sets
				vkCmdBindDescriptorSets(commandBuffers[id], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipelineLayout, static_cast<uint32_t>(fixedDescriptorSets.size()), static_cast<uint32_t>(frequentDescriptorSets.size()), frequentDescriptorSets.data(), 0, nullptr);

				// Draw indexed via the index buffer
				vkCmdDrawIndexed(commandBuffers[id], static_cast<uint32_t>(duckActor.mesh.indices.size()), 1, 0, 0, 0);
			}
		}

		// Draw the ui as well
		uiOverlay.draw(commandBuffers[id], id, swapchainFramebuffers[id]);
	}
	// End the render pass
	vkCmdEndRenderPass(commandBuffers[id]);

	// End the command buffer recording
	if (vkEndCommandBuffer(commandBuffers[id]) != VK_SUCCESS)
	{
		std::cerr << "Error: Could not end the recording of the command buffer." << std::endl;
		std::exit(-1);
	}
}