#pragma once
#include <vulkan/vulkan.h>
#include "Transform.h"
#include <vector>

/**
 * @brief Camera class
 * This includes transform and projection
*/
class Camera
{
public:
	// View
	Transform transform;
	glm::mat4 projection = glm::mat4(1.0f);

	// Vulkan specific
	VkDeviceSize projectionOffsetIntoBuffer = 0;
	VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;
	std::vector<VkDescriptorSet> descriptorSets;

public:
	// Defaulted Constructor
	Camera() = default;

	// Destroy method
	void destroy();

	// [Re] generation of descriptor sets
	void generateDSL(const bool& reset = false);
};

