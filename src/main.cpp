#include <iostream>
#include "Application.h"

/**
 * @brief Main func
 * @return
*/
int main()
{
	Application application;

	application.mainLoop();

	return 0;
}