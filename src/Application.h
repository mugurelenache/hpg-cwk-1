#pragma once

#include <vulkan/vulkan.h>
#include <string>
#include <vector>
#include <optional>
#include "Mesh.h"
#include "Vertex.h"
#include <unordered_map>
#include "VKC.h"
#include "Actor.h"
#include "Buffer.h"
#include "Light.h"
#include "Camera.h"
#include "UIOverlay.h"
#include "PushConstantShaderFeatures.h"

struct SwapchainSupportDetails
{
	VkSurfaceCapabilitiesKHR capabilities = {};
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

class Application
{
public:
	// Vulkan Specific Members
	const std::string VERSION_ID = "1.0.0";
	const std::string TITLE = "Vulkan Application";

	static constexpr bool debug = true;
	const uint32_t maxFramesInFlight = 2;
	uint32_t currentFrame = 0;

	std::vector<const char*> requiredValidationLayers =
	{
		"VK_LAYER_KHRONOS_validation"
	};

	std::vector<const char*> requiredDeviceExtensions =
	{
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};

	VkDebugUtilsMessengerEXT debugMessenger = VK_NULL_HANDLE;

	VkInstance instance = VK_NULL_HANDLE;
	VkSurfaceKHR surface = VK_NULL_HANDLE;

	VkPipeline graphicsPipeline;
	VkPipelineLayout graphicsPipelineLayout;
	VkRenderPass renderPass;

	// Framebuffers count = swapchain images count
	std::vector<VkFramebuffer> swapchainFramebuffers;

	// Same here, we have 1 cmd buffer per frame
	std::vector<VkCommandBuffer> commandBuffers;

	// Sync objects
	std::vector<VkSemaphore> imageAvailableSemaphores;
	std::vector<VkSemaphore> renderFinishedSemaphores;
	std::vector<VkFence> inFlightFences;
	std::vector<VkFence> fenceImagesInFlight;

	// Window state variable
	bool framebufferResized = false;

	VkImage depthImage;
	VkDeviceMemory depthImageMemory;
	VkImageView depthImageView;

	VkImage colorImage;
	VkDeviceMemory colorImageMemory;
	VkImageView colorImageView;

	// User specific
	std::vector<Mesh> duckMesh;
	Texture texture;
	Camera camera;
	Light light;
	Actor duckActor;
	PushConstantShaderFeatures pushConstants;
	UIOverlay uiOverlay;

public:
	Application();
	~Application();
	void mainLoop();

private:
	// General init functions
	void initializeVulkan();
	void initializeWindow();
	static void framebufferResizeCallback(GLFWwindow* window, int width, int height);

	// Abstraction over vulkan
	void sceneInit();

	///////////////////////////////////////////
	// Vulkan Specific Initialization functions

	// Method that creates the vulkan instance
	void createInstance();

	// Method that checks if the device has the validation layers
	bool checkValidationLayers();

	// Method that gets the required instance extensions (glfw + debug)
	std::vector<const char*> getExtensions();

	// Method that picks the physical
	void pickPhysicalDevice();

	// Utility functions for choosing a device
	uint32_t rateDevice(const VkPhysicalDevice& device);

	// Method that finds the necessary queue families
	QueueFamilyIndices findQueueFamilies(const VkPhysicalDevice& device);

	// Method that checks the extensions support of the device
	bool checkDeviceExtensionsSupport(const VkPhysicalDevice& device);

	// Queries the swapchain support details of the device
	SwapchainSupportDetails querySwapchainSupport(const VkPhysicalDevice& device);

	// Method for creating a logical device
	void createLogicalDevice();

	// Method that tries to create a surface to present on
	void createSurface();

	// Method to choose the extent given surface capabilities
	VkExtent2D chooseExtent(const VkSurfaceCapabilitiesKHR& capabilities);

	// Method that creates the swapchain
	void createSwapchain();
	// Method to create swapchain image views
	void createImageViews();

	// Method to create the geometry graphics pipeline
	void createGraphicsPipeline();
	// Method to create a renderpass
	void createRenderpass();

	// Method for creating the swapchain framebuffers
	void createFramebuffers();

	// Method for creating a general command pool
	void createCommandPool();

	// Method for creating the command buffers for each frame
	void createCommandBuffers();
	// Method for creating synchronization objects for scheduling
	void createSyncObjects();

	// Method to cleanup the old swapchain
	void cleanupOldSwapchain();

	// Method to recreate the swapchain and the other resources associated to it
	void recreateSwapchain();

	// Method to submit the work to the GPU
	void draw();

	// Method to create the uniform buffers for each frame
	void createUniformBuffers();

	// Method to update the uniform buffer for a frame
	void updateUniformBuffer(const uint32_t& imageID);

	// Method to create the general descriptor pool
	void createDescriptorPool();

	// Methods to create resources
	void createColorResources();
	void createDepthResources();
	void loadModel();

	// Method that maps the user uniforms to the buffers
	void mapUserDataToBuffers();

	// Method to [re]generate the descriptor sets & layout for the model
	void generateUserDataDSL(const bool& reset = false);

	// Method to destroy the
	void destroyUserData();

	// Method to update the command buffer of a frame
	void updateCommandBuffer(const uint32_t& id);
};