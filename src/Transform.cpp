#include "Transform.h"
#include <glm/gtc/matrix_transform.hpp>

/**
 * @brief Applies translation rotation scale
 * to generate the transform matrix
*/
void Transform::applyTRS()
{
	matrix = glm::mat4(1.0f);
	matrix = glm::translate(matrix, glm::vec3(position));
	matrix = glm::rotate(matrix, glm::radians(rotation.x), glm::vec3(1, 0, 0));
	matrix = glm::rotate(matrix, glm::radians(rotation.y), glm::vec3(0, 1, 0));
	matrix = glm::rotate(matrix, glm::radians(rotation.z), glm::vec3(0, 0, 1));
	matrix = glm::scale(matrix, glm::vec3(scale));
}
