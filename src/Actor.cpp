#include "Actor.h"
#include <iostream>
#include "VKC.h"

/**
 * @brief Method to destroy the actor
*/
void Actor::destroy()
{
	mesh.destroy();
	texture.destroy();
	vkDestroyDescriptorSetLayout(VKC::get().device, descriptorSetLayout, nullptr);
}


/**
 * @brief Method to [re]generate descriptor set layouts
 * @param reset
*/
void Actor::generateDSL(const bool& reset)
{
	if (reset)
	{
		vkDestroyDescriptorSetLayout(VKC::get().device, descriptorSetLayout, nullptr);
	}

	// Create the bindings
	std::vector<VkDescriptorSetLayoutBinding> bindings;
	{
		VkDescriptorSetLayoutBinding binding = {};
		binding.binding = 0;
		binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		binding.descriptorCount = 1;
		binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		binding.pImmutableSamplers = nullptr;
		bindings.push_back(binding);
	}

	{
		VkDescriptorSetLayoutBinding binding = {};
		binding.binding = 1;
		binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		binding.descriptorCount = 1;
		binding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
		binding.pImmutableSamplers = nullptr;
		bindings.push_back(binding);
	}

	{
		VkDescriptorSetLayoutBinding binding = {};
		binding.binding = 2;
		binding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		binding.descriptorCount = 1;
		binding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
		binding.pImmutableSamplers = nullptr;
		bindings.push_back(binding);
	}

	VkDescriptorSetLayoutCreateInfo dslCI = {};
	dslCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	dslCI.bindingCount = static_cast<uint32_t>(bindings.size());
	dslCI.pBindings = bindings.data();

	// Create the descriptor set layout
	if (vkCreateDescriptorSetLayout(VKC::get().device, &dslCI, nullptr, &descriptorSetLayout) != VK_SUCCESS)
	{
		std::cerr << "Error: cannot create descriptor set layout" << std::endl;
		std::exit(-1);
	}

	std::vector<VkDescriptorSetLayout> layouts(VKC::get().swapchainImages.size(), descriptorSetLayout);

	VkDescriptorSetAllocateInfo dsAI = {};
	dsAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	dsAI.descriptorPool = VKC::get().descriptorPool;
	dsAI.descriptorSetCount = static_cast<uint32_t>(VKC::get().swapchainImages.size());
	dsAI.pSetLayouts = layouts.data();

	descriptorSets.resize(VKC::get().swapchainImages.size());

	// Allocating the descriptor sets
	if (vkAllocateDescriptorSets(VKC::get().device, &dsAI, descriptorSets.data()) != VK_SUCCESS)
	{
		std::cerr << "Error: failed to allocate descriptor sets." << std::endl;
		std::exit(-1);
	}

	// Creating the writes and updating the descriptor sets
	for (size_t i = 0; i < descriptorSets.size(); i++)
	{
		std::vector<VkWriteDescriptorSet> descriptorSetWrites;
		//  TRANSFORM
		VkDescriptorBufferInfo descriptorBI = {};
		descriptorBI.buffer = VKC::get().uniformBuffers[i].buffer;
		descriptorBI.offset = transform.offsetIntoBuffer;
		descriptorBI.range = sizeof(Transform);

		{
			VkWriteDescriptorSet descriptorWrite = {};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstSet = descriptorSets[i];
			descriptorWrite.dstBinding = 0;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pBufferInfo = &descriptorBI;
			descriptorWrite.pImageInfo = nullptr;
			descriptorWrite.pTexelBufferView = nullptr;
			descriptorSetWrites.push_back(descriptorWrite);
		}

		// Material
		VkDescriptorBufferInfo descriptorMI = {};
		descriptorMI.buffer = VKC::get().uniformBuffers[i].buffer;
		descriptorMI.offset = mesh.material.offsetIntoBuffer;
		descriptorMI.range = sizeof(Material);

		{
			VkWriteDescriptorSet descriptorWrite = {};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstSet = descriptorSets[i];
			descriptorWrite.dstBinding = 1;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pBufferInfo = &descriptorMI;
			descriptorWrite.pImageInfo = nullptr;
			descriptorWrite.pTexelBufferView = nullptr;
			descriptorSetWrites.push_back(descriptorWrite);
		}

		// IMAGE DSW
		VkDescriptorImageInfo descriptorII = {};
		descriptorII.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		descriptorII.imageView = texture.textureImageView;
		descriptorII.sampler = texture.textureImageSampler;

		{
			VkWriteDescriptorSet descriptorWrite = {};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstSet = descriptorSets[i];
			descriptorWrite.dstBinding = 2;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pBufferInfo = nullptr;
			descriptorWrite.pImageInfo = &descriptorII;
			descriptorWrite.pTexelBufferView = nullptr;
			descriptorSetWrites.push_back(descriptorWrite);
		}

		vkUpdateDescriptorSets(VKC::get().device, static_cast<uint32_t>(descriptorSetWrites.size()), descriptorSetWrites.data(), 0, nullptr);
	}
}
