#include "Camera.h"
#include <iostream>
#include "VKC.h"

/**
 * @brief Destroys the descriptor set layout
*/
void Camera::destroy()
{
	vkDestroyDescriptorSetLayout(VKC::get().device, descriptorSetLayout, nullptr);
}

/**
 * @brief [RE]generate the descriptor sets and layouts
 * @param reset
*/
void Camera::generateDSL(const bool& reset)
{
	if (reset)
	{
		vkDestroyDescriptorSetLayout(VKC::get().device, descriptorSetLayout, nullptr);
	}

	// Creating the bindings for the vertex stages
	// Mainly we have the camera transform and the camera projection
	// uniforms that will be mapped in the shader
	std::vector<VkDescriptorSetLayoutBinding> bindings;
	{
		VkDescriptorSetLayoutBinding binding = {};
		binding.binding = 0;
		binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		binding.descriptorCount = 1;
		binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		binding.pImmutableSamplers = nullptr;
		bindings.push_back(binding);
	}
	{
		VkDescriptorSetLayoutBinding binding = {};
		binding.binding = 1;
		binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		binding.descriptorCount = 1;
		binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		binding.pImmutableSamplers = nullptr;
		bindings.push_back(binding);
	}

	// Create the descriptor set layout CI
	VkDescriptorSetLayoutCreateInfo dslCI = {};
	dslCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	dslCI.bindingCount = static_cast<uint32_t>(bindings.size());
	dslCI.pBindings = bindings.data();

	// Try to create the descriptor set layout
	if (vkCreateDescriptorSetLayout(VKC::get().device, &dslCI, nullptr, &descriptorSetLayout) != VK_SUCCESS)
	{
		std::cerr << "Error: cannot create descriptor set layout" << std::endl;
		std::exit(-1);
	}

	// Resize the layouts
	std::vector<VkDescriptorSetLayout> layouts(VKC::get().swapchainImages.size(), descriptorSetLayout);

	// Fill in the allocate info for the descriptor sets
	VkDescriptorSetAllocateInfo dsAI = {};
	dsAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	dsAI.descriptorPool = VKC::get().descriptorPool;
	dsAI.descriptorSetCount = static_cast<uint32_t>(VKC::get().swapchainImages.size());
	dsAI.pSetLayouts = layouts.data();

	descriptorSets.resize(VKC::get().swapchainImages.size());

	//  Try to allocate the descriptor sets
	if (vkAllocateDescriptorSets(VKC::get().device, &dsAI, descriptorSets.data()) != VK_SUCCESS)
	{
		std::cerr << "Error: failed to allocate descriptor sets." << std::endl;
		std::exit(-1);
	}

	// Update the descriptor sets via the descriptor writes
	for (size_t i = 0; i < descriptorSets.size(); i++)
	{
		std::vector<VkWriteDescriptorSet> descriptorSetWrites;
		//  TRANSFORM - VIEW
		VkDescriptorBufferInfo descriptorBI = {};
		descriptorBI.buffer = VKC::get().uniformBuffers[i].buffer;
		descriptorBI.offset = transform.offsetIntoBuffer;
		descriptorBI.range = sizeof(Transform);

		{
			VkWriteDescriptorSet descriptorWrite = {};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstSet = descriptorSets[i];
			descriptorWrite.dstBinding = 0;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pBufferInfo = &descriptorBI;
			descriptorWrite.pImageInfo = nullptr;
			descriptorWrite.pTexelBufferView = nullptr;
			descriptorSetWrites.push_back(descriptorWrite);
		}

		// Projection
		VkDescriptorBufferInfo descriptorPI = {};
		descriptorPI.buffer = VKC::get().uniformBuffers[i].buffer;
		descriptorPI.offset = projectionOffsetIntoBuffer;
		descriptorPI.range = sizeof(glm::mat4);

		{
			VkWriteDescriptorSet descriptorWrite = {};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstSet = descriptorSets[i];
			descriptorWrite.dstBinding = 1;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pBufferInfo = &descriptorPI;
			descriptorWrite.pImageInfo = nullptr;
			descriptorWrite.pTexelBufferView = nullptr;
			descriptorSetWrites.push_back(descriptorWrite);
		}

		vkUpdateDescriptorSets(VKC::get().device, static_cast<uint32_t>(descriptorSetWrites.size()), descriptorSetWrites.data(), 0, nullptr);
	}

}
