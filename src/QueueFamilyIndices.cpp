#include "QueueFamilyIndices.h"

/**
 * @brief Checks if all the graphics indices are there
 * @return
*/
bool QueueFamilyIndices::isComplete()
{
	return graphicsFamily.has_value() && presentFamily.has_value() && transferFamily.has_value();
}


/**
 * @brief Gets all the indices of the queues
 * @return
*/
std::vector<uint32_t> QueueFamilyIndices::getAllIndices()
{
	std::vector<uint32_t> indices = { graphicsFamily.value(), presentFamily.value(), transferFamily.value() };
	return indices;
}


/**
 * @brief Gets all unique indices as a set
 * @return
*/
std::set<uint32_t> QueueFamilyIndices::getAllUniqueIndices()
{
	std::set<uint32_t> uniqueQueueFamilies = { graphicsFamily.value(), presentFamily.value(), transferFamily.value() };
	return uniqueQueueFamilies;
}
