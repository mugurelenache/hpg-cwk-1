#pragma once

#include <vulkan/vulkan.h>
#include <vector>

namespace Utility
{
	// Choose surface format given the available formats
	VkSurfaceFormatKHR chooseSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);

	// Choose present mode given the available present modes
	VkPresentModeKHR choosePresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);

	// Finds the memory type, given the pyh device,  filter and the property flags
	uint32_t findMemoryType(VkPhysicalDevice& phyDevice, const uint32_t& memoryTypeFilter, const VkMemoryPropertyFlags& propertyFlags);

	// Creates a buffer
	void createBuffer(VkPhysicalDevice& phyDevice, VkDevice& device, VkBuffer& buffer, VkDeviceMemory& bufferMemory, const VkDeviceSize& size, const VkBufferUsageFlags& usage, const VkMemoryPropertyFlags& memoryProperties);

	// Creates an image
	void createImage(VkPhysicalDevice& phyDevice, VkDevice& device, VkImage& image, VkDeviceMemory& imageMemory, const uint32_t& width, const uint32_t& height, const uint32_t& depth, const uint32_t& mipLevels, const uint32_t& arrayLayers, const VkSampleCountFlagBits& numSamples, const VkFormat& format, const VkImageTiling& tiling, const VkImageUsageFlags& usage, const VkMemoryPropertyFlags& properties);

	// Creates an image view
	void createImageView(VkDevice& device, VkImageView& imageView, VkImage& image, const VkImageViewType& imageViewType, const VkFormat& format, const VkImageAspectFlags& aspectFlags, const uint32_t& levelCount, const uint32_t& layerCount);

	// Generates the mip maps
	void generateMipMaps(VkPhysicalDevice& phyDevice, VkDevice& device, VkCommandPool& commandPool, VkQueue& queue, VkImage& image, const VkFormat& format, const uint32_t& width, const uint32_t& height, const uint32_t& mipLevels);

	// Begins the command buffer (single time)
	VkCommandBuffer beginSingleTimeCommands(VkDevice& device, const VkCommandPool& commandPool);

	// Ends the single time command buffer
	void endSingleTimeCommands(VkDevice& device, VkCommandBuffer& commandBuffer, VkQueue& queue, VkCommandPool& commandPool);

	// Finds the supported format
	VkFormat findSupportedFormat(VkPhysicalDevice& phyDevice, const std::vector<VkFormat>& candidates, const VkImageTiling& tiling, const VkFormatFeatureFlags& featureFlags);

	// Finds the depth format
	VkFormat findDepthFormat(VkPhysicalDevice& phyDevice);

	// Checks if a format has a stencil component
	bool hasStencilComponent(const VkFormat& format);

	// Copies the buffer to another buffer
	void copyBuffer(VkDevice& device, VkCommandPool& commandPool, VkQueue& queue, VkBuffer& srcBuffer, VkBuffer& dstBuffer, const VkDeviceSize& size);

	// Copies the buffer to an image
	void copyBufferToImage(VkDevice& device, VkCommandPool& commandPool, VkQueue& queue, VkBuffer& buffer, VkImage& image, const uint32_t& width, const uint32_t& height);

	// Transitioning the image to a new layout
	void transitionImageLayout(VkDevice& device, VkCommandPool& commandPool, VkQueue& queue, VkImage& image, const VkFormat& format, const VkImageLayout& oldImageLayout, const VkImageLayout& newImageLayout, const uint32_t& mipLevels);

	// Calculates the uniform allocation size given a size
	VkDeviceSize calculateAllocSize(const VkDeviceSize& size);

	// Creates a shader module
	VkShaderModule createShaderModule(const std::vector<uint32_t>& source);

	// Checks the maximum multisampling count
	VkSampleCountFlagBits getMaxUsableSampleCount();

	// Inject-like func to debug callbacks
	VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData
	);

	// Generates create info for a debug messenger
	VkDebugUtilsMessengerCreateInfoEXT generateMessengerCreateInfo();

	// Creates the debug messenger
	VkResult createDebugMessenger(const VkInstance& instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, VkDebugUtilsMessengerEXT& debugMessenger);

	// Destroys the debug messenger
	void destroyDebugMessenger(const VkInstance& instance, VkDebugUtilsMessengerEXT& debugMessenger);
}