#pragma once
#include <optional>
#include <vector>
#include <set>

/**
 * @brief Queue family indices
*/
class QueueFamilyIndices
{
public:
	std::optional<uint32_t> graphicsFamily;
	std::optional<uint32_t> presentFamily;
	std::optional<uint32_t> transferFamily;

public:
	// Checks if everything exists
	bool isComplete();

	// Gets a vector of indices (i.e. the values)
	std::vector<uint32_t> getAllIndices();

	// Gets a unique set of all the indices
	std::set<uint32_t> getAllUniqueIndices();
};

