#include "Utility.h"
#include <iostream>
#include "VKC.h"

/**
 * @brief Chooses the best available format, preferably SRGB
 * @param availableFormats
 * @return
*/
VkSurfaceFormatKHR Utility::chooseSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats)
{
	for (const auto& availableFormat : availableFormats)
	{
		if (availableFormat.format == VK_FORMAT_B8G8R8A8_SRGB &&
			availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
		{
			return availableFormat;
		}
	}

	// No more ranking
	return availableFormats[0];
}


/**
 * @brief Chooses the best present mode
 * Primarily, we look for FIFO. Otherwise, we return immediate mode.
 * @param availablePresentModes
 * @return
*/
VkPresentModeKHR Utility::choosePresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes)
{
	for (const auto& mode : availablePresentModes)
	{
		if (mode == VK_PRESENT_MODE_FIFO_KHR)
		{
			return mode;
		}
	}

	std::cout << "Status: Resetting on VK_PRESENT_MODE_IMMEDIATE_KHR." << std::endl;
	// Fall-back on vsync
	return VK_PRESENT_MODE_IMMEDIATE_KHR;
}


/**
 * @brief Finds a suitable memory type for what we want to achieve
 * @param phyDevice
 * @param memoryTypeFilter
 * @param propertyFlags
 * @return
*/
uint32_t Utility::findMemoryType(VkPhysicalDevice& phyDevice, const uint32_t& memoryTypeFilter, const VkMemoryPropertyFlags& propertyFlags)
{
	VkPhysicalDeviceMemoryProperties memoryProperties;
	vkGetPhysicalDeviceMemoryProperties(phyDevice, &memoryProperties);

	for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; i++)
	{
		// Shifting through the counts
		if (memoryTypeFilter & (1 << i))
		{
			// If the property flags of that memory types are matching with the requested ones
			if ((memoryProperties.memoryTypes[i].propertyFlags & propertyFlags) == propertyFlags)
			{
				return i;
			}
		}
	}

	std::cerr << "Error: Could not find suitable memory type." << std::endl;
	std::exit(-1);
}


/**
 * @brief Creates a buffer (general func)
 * @param phyDevice
 * @param device
 * @param buffer
 * @param bufferMemory
 * @param size
 * @param usage
 * @param memoryProperties
*/
void Utility::createBuffer(VkPhysicalDevice& phyDevice, VkDevice& device, VkBuffer& buffer, VkDeviceMemory& bufferMemory, const VkDeviceSize& size, const VkBufferUsageFlags& usage, const VkMemoryPropertyFlags& memoryProperties)
{
	VkBufferCreateInfo bufferCI = {};
	bufferCI.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferCI.size = size;
	bufferCI.usage = usage;
	bufferCI.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	if (vkCreateBuffer(device, &bufferCI, nullptr, &buffer) != VK_SUCCESS)
	{
		std::cerr << "Error: cannot create buffer." << std::endl;
		std::exit(-1);
	}

	VkMemoryRequirements memoryReqs;
	vkGetBufferMemoryRequirements(device, buffer, &memoryReqs);

	VkMemoryAllocateInfo memAllocI = {};
	memAllocI.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memAllocI.allocationSize = memoryReqs.size;
	memAllocI.memoryTypeIndex = findMemoryType(phyDevice, memoryReqs.memoryTypeBits, memoryProperties);

	if (vkAllocateMemory(device, &memAllocI, nullptr, &bufferMemory) != VK_SUCCESS)
	{
		std::cerr << "Error: Could not allocate memory." << std::endl;
		std::exit(-1);
	}

	vkBindBufferMemory(device, buffer, bufferMemory, 0);
}


/**
 * @brief Creates an image and allocates its memory
 * @param phyDevice
 * @param device
 * @param image
 * @param imageMemory
 * @param width
 * @param height
 * @param depth
 * @param mipLevels
 * @param arrayLayers
 * @param numSamples
 * @param format
 * @param tiling
 * @param usage
 * @param properties
*/
void Utility::createImage(VkPhysicalDevice& phyDevice, VkDevice& device, VkImage& image, VkDeviceMemory& imageMemory, const uint32_t& width, const uint32_t& height, const uint32_t& depth, const uint32_t& mipLevels, const uint32_t& arrayLayers, const VkSampleCountFlagBits& numSamples, const VkFormat& format, const VkImageTiling& tiling, const VkImageUsageFlags& usage, const VkMemoryPropertyFlags& properties)
{
	VkImageCreateInfo imageInfo{};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = width;
	imageInfo.extent.height = height;
	imageInfo.extent.depth = depth;
	imageInfo.mipLevels = mipLevels;
	imageInfo.arrayLayers = arrayLayers;
	imageInfo.format = format;
	imageInfo.tiling = tiling;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.usage = usage;
	imageInfo.samples = numSamples;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	if (vkCreateImage(device, &imageInfo, nullptr, &image) != VK_SUCCESS)
	{
		std::cerr << "Error: Failed to create image." << std::endl;
		std::exit(-1);
	}

	VkMemoryRequirements memRequirements;
	vkGetImageMemoryRequirements(device, image, &memRequirements);

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = Utility::findMemoryType(phyDevice, memRequirements.memoryTypeBits, properties);

	if (vkAllocateMemory(device, &allocInfo, nullptr, &imageMemory) != VK_SUCCESS)
	{
		std::cerr << "Error: Failed to allocate image memory." << std::endl;
		std::exit(-1);
	}

	vkBindImageMemory(device, image, imageMemory, 0);
}


/**
 * @brief Creates an image view given an image
 * @param device
 * @param imageView
 * @param image
 * @param imageViewType
 * @param format
 * @param aspectFlags
 * @param levelCount
 * @param layerCount
*/
void Utility::createImageView(VkDevice& device, VkImageView& imageView, VkImage& image, const VkImageViewType& imageViewType, const VkFormat& format, const VkImageAspectFlags& aspectFlags, const uint32_t& levelCount, const uint32_t& layerCount)
{
	VkImageViewCreateInfo imageViewCI = {};
	imageViewCI.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	imageViewCI.image = image;
	imageViewCI.viewType = VK_IMAGE_VIEW_TYPE_2D;
	imageViewCI.format = format;
	imageViewCI.subresourceRange.aspectMask = aspectFlags;
	imageViewCI.subresourceRange.baseMipLevel = 0;
	imageViewCI.subresourceRange.levelCount = levelCount;
	imageViewCI.subresourceRange.baseArrayLayer = 0;
	imageViewCI.subresourceRange.layerCount = layerCount;

	if (vkCreateImageView(device, &imageViewCI, nullptr, &imageView) != VK_SUCCESS)
	{
		std::cerr << "Error: could not create image view." << std::endl;
		std::exit(-1);
	}
}


/**
 * @brief Generates the mip-maps of an image
 * @param phyDevice
 * @param device
 * @param commandPool
 * @param queue
 * @param image
 * @param format
 * @param width
 * @param height
 * @param mipLevels
*/
void Utility::generateMipMaps(VkPhysicalDevice& phyDevice, VkDevice& device, VkCommandPool& commandPool, VkQueue& queue, VkImage& image, const VkFormat& format, const uint32_t& width, const uint32_t& height, const uint32_t& mipLevels)
{
	VkFormatProperties formatProperties;
	vkGetPhysicalDeviceFormatProperties(phyDevice, format, &formatProperties);

	VkFilter blitFilter;

	if (formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT)
	{
		blitFilter = VK_FILTER_LINEAR;
	}
	else
	{
		std::cerr << "Error: Cannot use blit filter linear" << std::endl;
		std::exit(-1);
	}

	VkCommandBuffer commandBuffer = beginSingleTimeCommands(device, commandPool);

	// Basically, what we do is that we transfer the image into a new image
	// that simply halves at each step.
	// We wait for the work to be finished (via barriers)
	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.image = image;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.levelCount = 1;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;

	int32_t mipWidth = static_cast<int32_t>(width);
	int32_t mipHeight = static_cast<int32_t>(height);

	// Here the job is done
	for (uint32_t i = 1; i < mipLevels; i++)
	{
		barrier.subresourceRange.baseMipLevel = i - 1;
		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

		vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);

		// Blitting to half an image
		VkImageBlit blit = {};
		blit.srcOffsets[0] = { 0, 0, 0 };
		blit.srcOffsets[1] = { mipWidth, mipHeight, 1 };
		blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		blit.srcSubresource.mipLevel = i - 1;
		blit.srcSubresource.baseArrayLayer = 0;
		blit.srcSubresource.layerCount = 1;
		blit.dstOffsets[0] = { 0, 0, 0 };
		blit.dstOffsets[1] = { mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1 };
		blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		blit.dstSubresource.mipLevel = i;
		blit.dstSubresource.baseArrayLayer = 0;
		blit.dstSubresource.layerCount = 1;

		vkCmdBlitImage(commandBuffer, image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &blit, VK_FILTER_LINEAR);

		// Transfer mipmap to shader read only too
		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);

		// Exit checks mostly
		if (mipWidth > 1)
		{
			mipWidth /= 2;
		}

		if (mipHeight > 1)
		{
			mipHeight /= 2;
		}
	}

	// Transfer the last image too
	barrier.subresourceRange.baseMipLevel = mipLevels - 1;
	barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

	vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);

	endSingleTimeCommands(device, commandBuffer, queue, commandPool);
}


/**
 * @brief Begins a single time commands buffer
 * @param device
 * @param commandPool
 * @return
*/
VkCommandBuffer Utility::beginSingleTimeCommands(VkDevice& device, const VkCommandPool& commandPool)
{
	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool = commandPool;
	allocInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer;
	vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer(commandBuffer, &beginInfo);

	return commandBuffer;
}


/**
 * @brief Ends a single time command buffer
 * @param device
 * @param commandBuffer
 * @param queue
 * @param commandPool
*/
void Utility::endSingleTimeCommands(VkDevice& device, VkCommandBuffer& commandBuffer, VkQueue& queue, VkCommandPool& commandPool)
{
	vkEndCommandBuffer(commandBuffer);

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;

	vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(queue);

	vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
}


/**
 * @brief Finds the supported image format given some candidates
 * @param phyDevice
 * @param candidates
 * @param tiling
 * @param featureFlags
 * @return
*/
VkFormat Utility::findSupportedFormat(VkPhysicalDevice& phyDevice, const std::vector<VkFormat>& candidates, const VkImageTiling& tiling, const VkFormatFeatureFlags& featureFlags)
{
	for (auto& format : candidates)
	{
		VkFormatProperties properties;
		vkGetPhysicalDeviceFormatProperties(phyDevice, format, &properties);

		if (tiling == VK_IMAGE_TILING_OPTIMAL)
		{
			if ((properties.optimalTilingFeatures & featureFlags) == featureFlags)
			{
				return format;
			}
		}
		else if (tiling == VK_IMAGE_TILING_LINEAR)
		{
			if ((properties.linearTilingFeatures & featureFlags) == featureFlags)
			{
				return format;
			}
		}
	}

	std::cerr << "Error: cannot find a supported format." << std::endl;
	std::exit(-1);
}


/**
 * @brief Finds the best supported depth image format
 * @param phyDevice
 * @return
*/
VkFormat Utility::findDepthFormat(VkPhysicalDevice& phyDevice)
{
	return findSupportedFormat(phyDevice, { VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT, VK_FORMAT_D16_UNORM, VK_FORMAT_D16_UNORM_S8_UINT },
		VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}


/**
 * @brief Returns true if an image has a stencil component
 * @param format
 * @return
*/
bool Utility::hasStencilComponent(const VkFormat& format)
{
	return (format == VK_FORMAT_D32_SFLOAT_S8_UINT) || (format == VK_FORMAT_D24_UNORM_S8_UINT) || (format == VK_FORMAT_D16_UNORM_S8_UINT);
}


/**
 * @brief Copies a buffer into another buffer
 * @param device
 * @param commandPool
 * @param queue
 * @param srcBuffer
 * @param dstBuffer
 * @param size
*/
void Utility::copyBuffer(VkDevice& device, VkCommandPool& commandPool, VkQueue& queue, VkBuffer& srcBuffer, VkBuffer& dstBuffer, const VkDeviceSize& size)
{
	VkCommandBufferAllocateInfo cmdBufferAI = {};
	cmdBufferAI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	cmdBufferAI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	cmdBufferAI.commandPool = commandPool;
	cmdBufferAI.commandBufferCount = 1;

	VkCommandBuffer cmdBuffer;
	vkAllocateCommandBuffers(device, &cmdBufferAI, &cmdBuffer);

	VkCommandBufferBeginInfo cmdBufferBI = {};
	cmdBufferBI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	cmdBufferBI.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	// Copies the buffer into the new one
	vkBeginCommandBuffer(cmdBuffer, &cmdBufferBI);
	{
		VkBufferCopy bufferCopy = {};
		bufferCopy.srcOffset = 0;
		bufferCopy.dstOffset = 0;
		bufferCopy.size = size;

		vkCmdCopyBuffer(cmdBuffer, srcBuffer, dstBuffer, 1, &bufferCopy);
	}
	vkEndCommandBuffer(cmdBuffer);

	VkSubmitInfo submitI = {};
	submitI.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitI.commandBufferCount = 1;
	submitI.pCommandBuffers = &cmdBuffer;

	VkFence copyFence = VK_NULL_HANDLE;
	VkFenceCreateInfo fenceCI = {};
	fenceCI.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

	if (vkCreateFence(device, &fenceCI, nullptr, &copyFence) != VK_SUCCESS)
	{
		std::cerr << "Error: Cannot create copy fence." << std::endl;
		std::exit(-1);
	}

	// Submits the queue and waits until everything was transferred
	vkQueueSubmit(queue, 1, &submitI, copyFence);
	vkWaitForFences(device, 1, &copyFence, VK_TRUE, UINT64_MAX);

	vkFreeCommandBuffers(device, commandPool, 1, &cmdBuffer);
	vkDestroyFence(device, copyFence, nullptr);
}


/**
 * @brief Copies buffer to image
 * @param device
 * @param commandPool
 * @param queue
 * @param buffer
 * @param image
 * @param width
 * @param height
*/
void Utility::copyBufferToImage(VkDevice& device, VkCommandPool& commandPool, VkQueue& queue, VkBuffer& buffer, VkImage& image, const uint32_t& width, const uint32_t& height)
{
	VkCommandBuffer commandBuffer = Utility::beginSingleTimeCommands(device, commandPool);

	VkBufferImageCopy bufferImageCopy = {};
	bufferImageCopy.bufferOffset = 0;
	bufferImageCopy.bufferRowLength = width;
	bufferImageCopy.bufferImageHeight = height;
	bufferImageCopy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	bufferImageCopy.imageSubresource.mipLevel = 0;;
	bufferImageCopy.imageSubresource.baseArrayLayer = 0;
	bufferImageCopy.imageSubresource.layerCount = 1;
	bufferImageCopy.imageOffset = { 0 , 0 ,0 };
	bufferImageCopy.imageExtent = { width, height, 1 };

	vkCmdCopyBufferToImage(commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &bufferImageCopy);

	Utility::endSingleTimeCommands(device, commandBuffer, queue, commandPool);
}


/**
 * @brief Transitions the image layout
 * @param device
 * @param commandPool
 * @param queue
 * @param image
 * @param format
 * @param oldImageLayout
 * @param newImageLayout
 * @param mipLevels
*/
void Utility::transitionImageLayout(VkDevice& device, VkCommandPool& commandPool, VkQueue& queue, VkImage& image, const VkFormat& format, const VkImageLayout& oldImageLayout, const VkImageLayout& newImageLayout, const uint32_t& mipLevels)
{
	VkCommandBuffer commandBuffer = beginSingleTimeCommands(device, commandPool);

	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.srcAccessMask = 0;
	barrier.dstAccessMask = 0;
	barrier.oldLayout = oldImageLayout;
	barrier.newLayout = newImageLayout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = image;
	barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.levelCount = mipLevels;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;

	VkPipelineStageFlags srcStage;
	VkPipelineStageFlags dstStage;

	// Given the following possible transitions, we consider the barrier configs
	if (oldImageLayout == VK_IMAGE_LAYOUT_UNDEFINED && newImageLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
	{
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

		srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		dstStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	}
	else if (oldImageLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newImageLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
	{
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		dstStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	}
	else if (oldImageLayout == VK_IMAGE_LAYOUT_UNDEFINED && newImageLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
	{
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

		srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		dstStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
	}
	else
	{
		std::cerr << "Error: Invalid image layout transition specified." << std::endl;
		std::exit(-1);
	}

	// Set a barrier here to transfer it
	vkCmdPipelineBarrier(commandBuffer, srcStage, dstStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);
	endSingleTimeCommands(device, commandBuffer, queue, commandPool);
}


/**
 * @brief Calculates the alignment size for the uniform buffer
 * @param size
 * @return
*/
VkDeviceSize Utility::calculateAllocSize(const VkDeviceSize& size)
{
	VkDeviceSize alignment = VKC::get().minUniformBufferOffsetAlignment;
	return alignment * (size / alignment) + (size % alignment ? alignment : 0);
}


/**
 * @brief Creates a shader module
 * @param source
 * @return A handle to a shader module
*/
VkShaderModule Utility::createShaderModule(const std::vector<uint32_t>& source)
{
	VkShaderModuleCreateInfo shaderCI = {};
	shaderCI.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	shaderCI.codeSize = static_cast<uint32_t>(source.size()) * sizeof(uint32_t);
	shaderCI.pCode = source.data();

	VkShaderModule shaderModule;
	if (vkCreateShaderModule(VKC::get().device, &shaderCI, nullptr, &shaderModule) != VK_SUCCESS)
	{
		std::cerr << "Error: cannot create shader module." << std::endl;
		std::exit(-1);
	}

	return shaderModule;
}

/**
 * @brief Gets the maximum multi sample count (fixed to a max of 4bits)
 * @return
*/
VkSampleCountFlagBits Utility::getMaxUsableSampleCount()
{
	VkPhysicalDeviceProperties physicalDeviceProperties;
	vkGetPhysicalDeviceProperties(VKC::get().physicalDevice, &physicalDeviceProperties);

	VkSampleCountFlags counts = physicalDeviceProperties.limits.framebufferColorSampleCounts & physicalDeviceProperties.limits.framebufferDepthSampleCounts;

	// Although we can use more samples, my chosen max is 4 for performance reasons
	if (counts & VK_SAMPLE_COUNT_4_BIT)
	{
		return VK_SAMPLE_COUNT_4_BIT;
	}
	if (counts & VK_SAMPLE_COUNT_2_BIT)
	{
		return VK_SAMPLE_COUNT_2_BIT;
	}

	return VK_SAMPLE_COUNT_1_BIT;
}


/**
 * @brief Callback for debug messenger
 * @param messageSeverity
 * @param messageType
 * @param pCallbackData
 * @param pUserData
 * @return VK_FALSE
*/
VKAPI_ATTR VkBool32 VKAPI_CALL Utility::debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData)
{
	// Filtering messages
	if (messageType == VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT)
	{
		std::cerr << std::endl;
		std::cerr << "Validation layer: " << pCallbackData->pMessage << std::endl;
		std::cerr << std::endl;
	}

	return VK_FALSE;
}


/**
 * @brief Generates a VkDebugUtilsMessengerCreateInfoEXT
 * @return VkDebugUtilsMessengerCreateInfoEXT
*/
VkDebugUtilsMessengerCreateInfoEXT Utility::generateMessengerCreateInfo()
{
	VkDebugUtilsMessengerCreateInfoEXT messengerCI = {};
	messengerCI.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
	messengerCI.messageSeverity =
		VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
		VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
		VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;

	messengerCI.messageType =
		VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
		VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
		VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

	messengerCI.pfnUserCallback = Utility::debugCallback;
	return messengerCI;
}


/**
 * @brief Creates the debug messenger
 * @param pCreateInfo
 * @return result
*/
VkResult Utility::createDebugMessenger(const VkInstance& instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, VkDebugUtilsMessengerEXT& debugMessenger)
{
	// Getting a ptr to a func to created debug messenger
	auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");

	if (!func)
	{
		std::cerr << "Error: could not find extension including PFN to vkCreateDebugUtilsMessengerEXT()" << std::endl;
		std::exit(-1);
	}

	// If the func has not succeeded
	return func(instance, pCreateInfo, nullptr, &debugMessenger);
}


/**
 * @brief Destroys the debug messenger
*/
void Utility::destroyDebugMessenger(const VkInstance& instance, VkDebugUtilsMessengerEXT& debugMessenger)
{
	// Getting a ptr to a func to destroy debug messenger
	auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");

	if (!func)
	{
		std::cerr << "Error: could not find extension including PFN to vkDestroyDebugUtilsMessengerEXT()" << std::endl;
		std::exit(-1);
	}

	func(instance, debugMessenger, nullptr);
}