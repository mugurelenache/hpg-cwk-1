#include "Mesh.h"
#include "Utility.h"
#include <iostream>
#include <tiny_obj_loader.h>

/**
 * @brief Mesh default constructor
 * @param vertices
 * @param indices
*/
Mesh::Mesh(const std::vector<Vertex>& vertices, const std::vector<uint32_t>& indices)
{
	this->vertices = vertices;
	this->indices = indices;
}


/**
 * @brief Destroy the buffers
*/
void Mesh::destroy()
{
	vertexBuffer.destroy();
	indexBuffer.destroy();
}


/**
 * @brief Load the obj file
 * @param path
 * @return
*/
std::vector<Mesh> Mesh::loadObj(const fs::path& path)
{
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warn;
	std::string err;

	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, path.generic_string().c_str(), path.parent_path().generic_string().c_str()))
	{
		std::cerr << "Error: cannot load object " << path.c_str() << std::endl;
		std::exit(-1);
	}

	std::vector<Mesh> meshes;

	// For each shape, create the mesh
	for (const auto& shape : shapes)
	{
		std::unordered_map<Vertex, uint32_t> uniqueVertices;
		std::vector<Vertex> vertexData;
		std::vector<uint32_t> indexData;

		// Wrong assumption but we load one mtl per shape
		// The correct one would load the material per face instead

		Material mat;
		if (shape.mesh.material_ids.size())
		{
			auto& tmat = materials[shape.mesh.material_ids[0]];

			mat.ambient = glm::vec4(tmat.ambient[0], tmat.ambient[1], tmat.ambient[2], 1.0f);
			mat.diffuse = glm::vec4(tmat.diffuse[0], tmat.diffuse[1], tmat.diffuse[2], 1.0f);
			mat.specular = glm::vec4(tmat.specular[0], tmat.specular[1], tmat.specular[2], 1.0f);
			mat.specularExponent = tmat.shininess;
		}

		// Indexing into the vertex buffer
		for (const auto& index : shape.mesh.indices)
		{
			Vertex vertex;

			vertex.pos.x = attrib.vertices[3 * index.vertex_index + 0];
			vertex.pos.y = attrib.vertices[3 * index.vertex_index + 1];
			vertex.pos.z = attrib.vertices[3 * index.vertex_index + 2];
			vertex.pos.w = 1.0f;

			vertex.normal.x = attrib.normals[3 * index.normal_index + 0];
			vertex.normal.y = attrib.normals[3 * index.normal_index + 1];
			vertex.normal.z = attrib.normals[3 * index.normal_index + 2];
			vertex.normal.w = 1.0f;

			vertex.texCoord.x = attrib.texcoords[2 * index.texcoord_index + 0];
			vertex.texCoord.y = 1.0f - attrib.texcoords[2 * index.texcoord_index + 1];

			vertex.color = glm::vec4(1.0f);

			// Unique index buffers checking
			if (uniqueVertices.count(vertex) == 0)
			{
				uniqueVertices[vertex] = static_cast<uint32_t>(vertexData.size());
				vertexData.push_back(vertex);
			}

			indexData.push_back(uniqueVertices[vertex]);
		}

		meshes.emplace_back(Mesh(vertexData, indexData));
		meshes.back().material = mat;
	}

	return meshes;
}


/**
 * @brief Generates a simple triangle mesh
 * including the vertex & index buffers
 * @return
*/
Mesh Mesh::generateTriangle()
{
	std::vector<Vertex> vertices;
	Vertex v1 = Vertex(glm::vec4(0.0f, 0.5f, 0.0f, 1.0f), glm::vec4(1.0f, 0.0f, 0.0f, 1.0f), glm::vec4(0.0f, 0.0f, 1.0f, 1.0f), glm::vec2(0.5f, 1.0f));
	Vertex v2 = Vertex(glm::vec4(-0.5f, -0.5f, 0.0f, 1.0f), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f), glm::vec4(0.0f, 0.0f, 1.0f, 1.0f), glm::vec2(0.0f, 0.0f));
	Vertex v3 = Vertex(glm::vec4(0.5f, -0.5f, 0.0f, 1.0f), glm::vec4(0.0f, 0.0f, 1.0f, 1.0f), glm::vec4(0.0f, 0.0f, 1.0f, 1.0f), glm::vec2(1.0f, 0.0f));

	vertices.push_back(v1);
	vertices.push_back(v2);
	vertices.push_back(v3);

	std::vector<uint32_t> indices = { 0, 1, 2 };

	return Mesh(vertices, indices);
}


/**
 * @brief Generates the vertex and index buffers from the data
*/
void Mesh::generateBuffers()
{
	// Create the vertex buffer
	{
		VkDeviceSize bufferSize = sizeof(vertices[0]) * vertices.size();

		Buffer stagingBuffer;

		stagingBuffer.create(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

		stagingBuffer.bind();
		stagingBuffer.map(bufferSize, 0);

		stagingBuffer.populateWith(vertices.data(), bufferSize);

		vertexBuffer.create(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		vertexBuffer.bind();

		stagingBuffer.copyInto(vertexBuffer, stagingBuffer.size);

		stagingBuffer.destroy();
	}

	// Create the index buffer
	{
		VkDeviceSize bufferSize = sizeof(indices[0]) * indices.size();

		Buffer stagingBuffer;

		stagingBuffer.create(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

		stagingBuffer.bind();
		stagingBuffer.map(bufferSize, 0);

		stagingBuffer.populateWith(indices.data(), bufferSize);

		indexBuffer.create(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		indexBuffer.bind();

		stagingBuffer.copyInto(indexBuffer, stagingBuffer.size);

		stagingBuffer.destroy();
	}
}
