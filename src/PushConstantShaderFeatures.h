#pragma once
#include <vulkan/vulkan.h>

/**
 * @brief Push Constant Shader Features block
 *
 * Used for turning on/off stages in the renderer
*/
class PushConstantShaderFeatures
{
public:
	uint32_t objectEnabled = true;
	uint32_t texturingEnabled = true;
	uint32_t lightingEnabled = true;

public:
	PushConstantShaderFeatures() = default;
};

