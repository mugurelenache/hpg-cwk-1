#include "UIOverlay.h"
#include "Utility.h"
#include "VKC.h"
#include <iostream>
#include <filesystem>
#include <glm/gtc/type_ptr.hpp>
#include "Application.h"
#include <imgui_internal.h>
#include <backends/imgui_impl_glfw.h>

namespace fs = std::filesystem;

#ifndef MAIN_DIR
#define MAIN_DIR std::filesystem::current_path()
#endif

/**
 * @brief Constructor for the UI Overlay
*/
UIOverlay::UIOverlay()
{
	ImGui::CreateContext();
}

/**
 * @brief Method to destroy the UI allocated resources
*/
void UIOverlay::destroy()
{
	ImGui::DestroyContext();

	// Destroy each buffer created
	for (uint32_t i = 0; i < vertexBuffers.size(); i++)
	{
		vertexBuffers[i].destroy();
		indexBuffers[i].destroy();
	}

	// destroy the fonts
	vkDestroyImageView(VKC::get().device, fontTextureView, nullptr);
	vkDestroyImage(VKC::get().device, fontTextureImage, nullptr);
	vkFreeMemory(VKC::get().device, fontTextureMemory, nullptr);
	vkDestroySampler(VKC::get().device, fontTextureSampler, nullptr);

	// destroy the descriptor set layouts
	for (auto& dsl : descriptorSetLayouts)
	{
		vkDestroyDescriptorSetLayout(VKC::get().device, dsl, nullptr);
	}

	// Destroy the descriptor pool
	vkDestroyDescriptorPool(VKC::get().device, descriptorPool, nullptr);

	// Destroy the pipeline & layout
	vkDestroyPipelineLayout(VKC::get().device, uiGraphicsPipelineLayout, nullptr);
	vkDestroyPipeline(VKC::get().device, uiGraphicsPipeline, nullptr);
}


/**
 * @brief Imgui Initialization
*/
void UIOverlay::initialize()
{
	ImGui_ImplGlfw_InitForVulkan(VKC::get().window, true);

	// Sets the imgui settings
	ImGuiIO& io = ImGui::GetIO();
	ImGui::StyleColorsDark();
	io.IniFilename = nullptr;
}

/**
 * @brief Generate the resources for the UI
 * That will load a font & perform staging, as well as
 * creating the buffers necessary
*/
void UIOverlay::acquireResources()
{
	ImGuiIO& io = ImGui::GetIO();

	unsigned char* fontData;
	int textureWidth;
	int textureHeight;
	io.Fonts->GetTexDataAsRGBA32(&fontData, &textureWidth, &textureHeight);

	VkDeviceSize textureSize = textureWidth * textureHeight * 4;
	VkFormat format = VK_FORMAT_R8G8B8A8_UNORM;

	// Creating an image & an image view for the font
	Utility::createImage(VKC::get().physicalDevice, VKC::get().device, fontTextureImage, fontTextureMemory, textureWidth, textureHeight, 1, 1, 1, VK_SAMPLE_COUNT_1_BIT, format, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	Utility::createImageView(VKC::get().device, fontTextureView, fontTextureImage, VK_IMAGE_VIEW_TYPE_2D, format, VK_IMAGE_ASPECT_COLOR_BIT, 1, 1);

	// Preparing staging buffer for copy
	Buffer stagingBuffer;
	stagingBuffer.create(textureSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	stagingBuffer.bind();
	stagingBuffer.map(textureSize);
	stagingBuffer.populateWith(reinterpret_cast<void*>(fontData), textureSize);
	stagingBuffer.unmap();

	// Perform image transition on the font image
	Utility::transitionImageLayout(VKC::get().device, VKC::get().commandPool, VKC::get().transferQueue, fontTextureImage, format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1);

	// Copy the staging buffer with the font into the font image
	Utility::copyBufferToImage(VKC::get().device, VKC::get().commandPool, VKC::get().transferQueue, stagingBuffer.buffer, fontTextureImage, textureWidth, textureHeight);

	// Transition the image
	Utility::transitionImageLayout(VKC::get().device, VKC::get().commandPool, VKC::get().transferQueue, fontTextureImage, format, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 1);

	// Free the buffer
	stagingBuffer.destroy();

	// Creating the texture sampler
	{
		VkSamplerCreateInfo samplerCI = {};
		samplerCI.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerCI.minFilter = VK_FILTER_LINEAR;
		samplerCI.magFilter = VK_FILTER_LINEAR;
		samplerCI.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerCI.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerCI.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerCI.anisotropyEnable = VK_FALSE;

		VkPhysicalDeviceProperties properties = {};
		vkGetPhysicalDeviceProperties(VKC::get().physicalDevice, &properties);

		samplerCI.maxAnisotropy = 1.0f;

		samplerCI.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		samplerCI.minLod = -1000.0f;
		samplerCI.maxLod = 1000.0f;

		if (vkCreateSampler(VKC::get().device, &samplerCI, nullptr, &fontTextureSampler) != VK_SUCCESS)
		{
			std::cerr << "Error: Could not create texture sampler." << std::endl;
			std::exit(-1);
		}
	}

	// Resizes the buffers
	vertexBuffers.resize(VKC::get().swapchainImages.size());
	indexBuffers.resize(VKC::get().swapchainImages.size());
	oldVertexCounts.resize(vertexBuffers.size(), 0);
	oldIndexCounts.resize(indexBuffers.size(), 0);
}


/**
 * @brief Creates the UI specific descriptor pool
*/
void UIOverlay::createDescriptorPool()
{
	// This may have at most 100 samplers
	std::vector<VkDescriptorPoolSize> poolSizes =
	{
		{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, static_cast<uint32_t>(10) },
	};

	// A pool with max 100 sets
	VkDescriptorPoolCreateInfo poolCI = {};
	poolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolCI.maxSets = 100;
	poolCI.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
	poolCI.pPoolSizes = poolSizes.data();

	// Try to create a descriptor pool
	if (vkCreateDescriptorPool(VKC::get().device, &poolCI, nullptr, &descriptorPool) != VK_SUCCESS)
	{
		std::cerr << "Error: could not create descriptor pool." << std::endl;
		std::exit(-1);
	}
}


/**
 * @brief Preparing the descriptor sets & layouts
*/
void UIOverlay::prepareDescriptorSets()
{
	// Preparing the descriptor sets layout bindings
	// mainly considering image samplers
	std::vector<VkDescriptorSetLayoutBinding> descriptorSetLayoutBindings;
	{
		VkDescriptorSetLayoutBinding binding = {};
		binding.binding = 0;
		binding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		binding.descriptorCount = 1;
		binding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
		binding.pImmutableSamplers = nullptr;
		descriptorSetLayoutBindings.push_back(binding);
	}

	// Setting up the layouts with what was pushed above
	VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCI = {};
	descriptorSetLayoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	descriptorSetLayoutCI.bindingCount = static_cast<uint32_t>(descriptorSetLayoutBindings.size());
	descriptorSetLayoutCI.pBindings = descriptorSetLayoutBindings.data();

	// Resizing the descriptor set layouts
	descriptorSetLayouts.resize(VKC::get().swapchainImages.size());
	for (size_t i = 0; i < VKC::get().swapchainImages.size(); i++)
	{
		// Creates the descriptor set layout with the specified bindings
		VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;

		if (vkCreateDescriptorSetLayout(VKC::get().device, &descriptorSetLayoutCI, nullptr, &descriptorSetLayouts[i]) != VK_SUCCESS)
		{
			std::cerr << "Error: cannot create descriptor set layout" << std::endl;
			std::exit(-1);
		}
	}

	// Creates the allocation info for the descriptor set
	VkDescriptorSetAllocateInfo dsAI = {};
	dsAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	dsAI.descriptorPool = descriptorPool;
	dsAI.descriptorSetCount = static_cast<uint32_t>(descriptorSetLayouts.size());
	dsAI.pSetLayouts = descriptorSetLayouts.data();

	// Resize the descriptor sets
	descriptorSets.resize(descriptorSetLayouts.size());
	// Allocating descriptor sets within the array
	if (vkAllocateDescriptorSets(VKC::get().device, &dsAI, descriptorSets.data()) != VK_SUCCESS)
	{
		std::cerr << "Error: failed to allocate descriptor sets." << std::endl;
		std::exit(-1);
	}

	// Updating the descriptor sets via writes
	for (auto& descriptorSet : descriptorSets)
	{
		std::vector<VkWriteDescriptorSet> descriptorSetWrites;
		// IMAGE DSW
		// Mainly we have set x, binding 0 as a combined image sampler
		VkDescriptorImageInfo fontDescriptorII = {};
		fontDescriptorII.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		fontDescriptorII.imageView = fontTextureView;
		fontDescriptorII.sampler = fontTextureSampler;

		{
			VkWriteDescriptorSet descriptorWrite = {};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstSet = descriptorSet;
			descriptorWrite.dstBinding = 0;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pBufferInfo = nullptr;
			descriptorWrite.pImageInfo = &fontDescriptorII;
			descriptorWrite.pTexelBufferView = nullptr;
			descriptorSetWrites.push_back(descriptorWrite);
		}
		// Update the writes in bulk
		vkUpdateDescriptorSets(VKC::get().device, static_cast<uint32_t>(descriptorSetWrites.size()), descriptorSetWrites.data(), 0, nullptr);
	}
}


/**
 * @brief Prepare the graphics pipeline for the UI rendering
 * @param pipelineCache
 * @param renderPass
*/
void UIOverlay::preparePipeline(const VkPipelineCache& pipelineCache, const VkRenderPass& renderPass)
{
	fs::path p = MAIN_DIR;

	p /= VKC::get().shadersPath;
	p /= "imgui";

	std::string vsName = "vs.vert";
	std::string fsName = "fs.frag";

	fs::path vsPath = p / vsName;
	fs::path fsPath = p / fsName;

	auto& shaderCompiler = VKC::get().shaderCompiler;

	// Within the shader directory
	// We perform online compilation per pipeline creation
	auto vsShaderCode = shaderCompiler.loadShader(vsPath.string());
	auto fsShaderCode = shaderCompiler.loadShader(fsPath.string());
	auto vsSPV = shaderCompiler.compileToSPIRV(vsName, shaderc_glsl_vertex_shader, vsShaderCode);
	auto fsSPV = shaderCompiler.compileToSPIRV(fsName, shaderc_glsl_fragment_shader, fsShaderCode);

	// Create the shader modules & their corresponding shader stages CIs
	VkShaderModule vsModule = Utility::createShaderModule(vsSPV);
	VkShaderModule fsModule = Utility::createShaderModule(fsSPV);

	VkPipelineShaderStageCreateInfo vsStageCI = {};
	vsStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vsStageCI.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vsStageCI.module = vsModule;
	vsStageCI.pName = "main";

	VkPipelineShaderStageCreateInfo fsStageCI = {};
	fsStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fsStageCI.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fsStageCI.module = fsModule;
	fsStageCI.pName = "main";

	std::vector<VkPipelineShaderStageCreateInfo> shaderStages = { vsStageCI, fsStageCI };

	// Vertex input stage ----------------------------
	// Create specific vertex input attribute descriptions
	std::vector<VkVertexInputAttributeDescription> vertexAttributesDescriptions(3);
	{
		vertexAttributesDescriptions[0].binding = 0;
		vertexAttributesDescriptions[0].location = 0;
		vertexAttributesDescriptions[0].format = VK_FORMAT_R32G32_SFLOAT;
		vertexAttributesDescriptions[0].offset = offsetof(ImDrawVert, pos);

		vertexAttributesDescriptions[1].binding = 0;
		vertexAttributesDescriptions[1].location = 1;
		vertexAttributesDescriptions[1].format = VK_FORMAT_R32G32_SFLOAT;
		vertexAttributesDescriptions[1].offset = offsetof(ImDrawVert, uv);

		vertexAttributesDescriptions[1].binding = 0;
		vertexAttributesDescriptions[2].location = 2;
		vertexAttributesDescriptions[2].format = VK_FORMAT_R8G8B8A8_UNORM;
		vertexAttributesDescriptions[2].offset = offsetof(ImDrawVert, col);
	}

	// Create input binding
	VkVertexInputBindingDescription vertexInputBindingDescription{};
	vertexInputBindingDescription.binding = 0;
	vertexInputBindingDescription.stride = sizeof(ImDrawVert);
	vertexInputBindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	// Create the vertex input state CI, filling it with the data gathered before
	VkPipelineVertexInputStateCreateInfo vertexInputCI = {};
	vertexInputCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputCI.vertexBindingDescriptionCount = 1;
	vertexInputCI.pVertexBindingDescriptions = &vertexInputBindingDescription;
	vertexInputCI.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertexAttributesDescriptions.size());
	vertexInputCI.pVertexAttributeDescriptions = vertexAttributesDescriptions.data();

	// Input assembly stage ----------------------------
	VkPipelineInputAssemblyStateCreateInfo inputAssemblyCI = {};
	inputAssemblyCI.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssemblyCI.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssemblyCI.primitiveRestartEnable = VK_FALSE;

	// Viewport state ----------------------------
	// Dynamic scissor and viewport
	VkPipelineViewportStateCreateInfo viewportCI = {};
	viewportCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportCI.viewportCount = 1;
	viewportCI.pViewports = nullptr;
	viewportCI.scissorCount = 1;
	viewportCI.pScissors = nullptr;

	// Rasterization State -------------------------
	// Rasterization settings just like in the original graphics pipeline
	VkPipelineRasterizationStateCreateInfo rasterizationCI = {};
	rasterizationCI.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizationCI.depthClampEnable = VK_FALSE;
	rasterizationCI.rasterizerDiscardEnable = VK_FALSE;
	rasterizationCI.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizationCI.cullMode = VK_CULL_MODE_NONE;
	rasterizationCI.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	rasterizationCI.depthBiasEnable = VK_FALSE;
	rasterizationCI.lineWidth = 1.0f;

	// Multi sampling State  ---------------------------
	// Just like the original geometry pipeline, this one has same multi sampling
	// feature, as both of them are using the same renderpass
	VkPipelineMultisampleStateCreateInfo multisampleCI = {};
	multisampleCI.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampleCI.rasterizationSamples = VKC::get().msaaSamples;
	multisampleCI.sampleShadingEnable = VK_FALSE;

	// Depth Stencil State -----------------------
	// We're not writing the depth buffer, neither test it
	// this is because we need to draw the transparent ui as an overlay
	// and we need to have transparency orders
	VkPipelineDepthStencilStateCreateInfo depthStencilCI = {};
	depthStencilCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthStencilCI.depthTestEnable = VK_FALSE;
	depthStencilCI.depthWriteEnable = VK_FALSE;
	depthStencilCI.depthCompareOp = VK_COMPARE_OP_LESS;
	depthStencilCI.depthBoundsTestEnable = VK_FALSE;
	depthStencilCI.minDepthBounds = 0.0f;
	depthStencilCI.maxDepthBounds = 1.0f;
	depthStencilCI.stencilTestEnable = VK_FALSE;
	depthStencilCI.front = {};
	depthStencilCI.back = {};

	// Color Blend State -------------------------
	// We perform blending as we have transparent triangles in the UI
	// src alpha, 1 - src alpha
	VkPipelineColorBlendAttachmentState colorAttachment = {};
	colorAttachment.blendEnable = VK_TRUE;
	colorAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	colorAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	colorAttachment.colorBlendOp = VK_BLEND_OP_ADD;
	colorAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	colorAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

	// Blend state on the color attachment
	VkPipelineColorBlendStateCreateInfo colorBlendCI = {};
	colorBlendCI.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlendCI.attachmentCount = 1;
	colorBlendCI.pAttachments = &colorAttachment;

	// Dynamic State -----------------------------
	std::vector<VkDynamicState> dynamicStates =
	{
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_SCISSOR
	};

	// Filling in the dynamic state CI with the data above
	VkPipelineDynamicStateCreateInfo dynamicCI = {};
	dynamicCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicCI.dynamicStateCount = static_cast<uint32_t>(dynamicStates.size());
	dynamicCI.pDynamicStates = dynamicStates.data();

	// We need push constants for imgui
	VkPushConstantRange pushConstantRange = {};
	pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
	pushConstantRange.size = sizeof(PushConstantsBlock);
	pushConstantRange.offset = 0;

	// Creating the pipeline layout
	VkPipelineLayoutCreateInfo pipelineLayoutCI = {};
	pipelineLayoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCI.setLayoutCount = static_cast<uint32_t>(descriptorSetLayouts.size());
	pipelineLayoutCI.pSetLayouts = descriptorSetLayouts.data();
	pipelineLayoutCI.pushConstantRangeCount = 1;
	pipelineLayoutCI.pPushConstantRanges = &pushConstantRange;

	// Try to create the layout
	if (vkCreatePipelineLayout(VKC::get().device, &pipelineLayoutCI, nullptr, &uiGraphicsPipelineLayout) != VK_SUCCESS)
	{
		std::cerr << "Error: Cannot create pipeline layout." << std::endl;
		std::exit(-1);
	}

	// Pipeline CI
	VkGraphicsPipelineCreateInfo pipelineCI = {};
	pipelineCI.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineCI.stageCount = static_cast<uint32_t>(shaderStages.size());
	pipelineCI.pStages = shaderStages.data();
	pipelineCI.pVertexInputState = &vertexInputCI;
	pipelineCI.pInputAssemblyState = &inputAssemblyCI;
	pipelineCI.pTessellationState = nullptr;
	pipelineCI.pViewportState = &viewportCI;
	pipelineCI.pRasterizationState = &rasterizationCI;
	pipelineCI.pMultisampleState = &multisampleCI;
	pipelineCI.pDepthStencilState = &depthStencilCI;
	pipelineCI.pColorBlendState = &colorBlendCI;
	pipelineCI.pDynamicState = &dynamicCI;
	pipelineCI.layout = uiGraphicsPipelineLayout;
	pipelineCI.renderPass = renderPass;
	pipelineCI.subpass = 0;
	pipelineCI.basePipelineHandle = VK_NULL_HANDLE;
	pipelineCI.basePipelineIndex = -1;

	// Try to create the graphics pipeline
	if (vkCreateGraphicsPipelines(VKC::get().device, pipelineCache, 1, &pipelineCI, nullptr, &uiGraphicsPipeline) != VK_NULL_HANDLE)
	{
		std::cerr << "Error: Failed to create graphics pipeline." << std::endl;
		std::exit(-1);
	}

	// Clear shader modules as not needed
	vkDestroyShaderModule(VKC::get().device, vsModule, nullptr);
	vkDestroyShaderModule(VKC::get().device, fsModule, nullptr);
}

/**
 * @brief Updates the vertex & index buffers
 * @return Whether the buffers were updated
*/
bool UIOverlay::update()
{
	bool updated = false;

	ImDrawData* imDrawData = ImGui::GetDrawData();

	if (!imDrawData)
	{
		return false;
	}

	VkDeviceSize vertexBufferSize = imDrawData->TotalVtxCount * sizeof(ImDrawVert);
	VkDeviceSize indexBufferSize = imDrawData->TotalIdxCount * sizeof(ImDrawIdx);

	// Update only if we have new changes in the vtx/idx buffer.
	// No need to update if no changes have been made
	if (vertexBufferSize == 0 || indexBufferSize == 0)
	{
		return false;
	}

	for (uint32_t i = 0; i < vertexBuffers.size(); i++)
	{
		auto& vertexBuffer = vertexBuffers[i];
		auto& indexBuffer = indexBuffers[i];
		auto& oldVertexCount = oldVertexCounts[i];
		auto& oldIndexCount = oldIndexCounts[i];

		//  If the buffer was not created or needs to be resized
		if ((vertexBuffer.buffer == VK_NULL_HANDLE) || (oldVertexCount != imDrawData->TotalVtxCount))
		{
			// Update old vertex count so we know how many vertices have been drawn
			// on this frame
			oldVertexCount = imDrawData->TotalVtxCount;

			// Automatically checks if it was mapped so we unmap it for future usage
			// This will not affect the first upload
			vertexBuffer.unmap();
			vertexBuffer.destroy();

			// Creates the buffer to be accessible to the host so we can upload data directly
			vertexBuffer.create(vertexBufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
			vertexBuffer.bind();
			vertexBuffer.map(vertexBufferSize);
			updated = true;
		}

		//  If the buffer was not created or needs to be resized
		if ((indexBuffer.buffer == VK_NULL_HANDLE) || (oldIndexCount != imDrawData->TotalIdxCount))
		{
			// Update old index count so we know how many indices have been drawn
			// on this frame
			oldIndexCount = imDrawData->TotalIdxCount;

			// Unmap & destroy the old buffer to reallocate memory
			indexBuffer.unmap();
			indexBuffer.destroy();

			// Proceeds by creating a new index buffer, so we can upload vertices
			indexBuffer.create(indexBufferSize, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
			indexBuffer.bind();
			indexBuffer.map(indexBufferSize);
			updated = true;
		}

		// Currently, we know for sure that both buffers are mapped
		ImDrawVert* vtxMap = reinterpret_cast<ImDrawVert*>(vertexBuffer.mapping);
		ImDrawIdx* idxMap = reinterpret_cast<ImDrawIdx*>(indexBuffer.mapping);

		// Imgui command lists are split in multiple command lists
		// so we need to prepare each buffer as necessary
		for (int j = 0; j < imDrawData->CmdListsCount; j++)
		{
			const ImDrawList* cmdList = imDrawData->CmdLists[j];

			// Copy the data from within the current cmd list to the buffers
			memcpy(vtxMap, cmdList->VtxBuffer.Data,
				cmdList->VtxBuffer.Size * sizeof(ImDrawVert));

			memcpy(idxMap, cmdList->IdxBuffer.Data,
				cmdList->IdxBuffer.Size * sizeof(ImDrawIdx));

			// Offset the destination mappings
			vtxMap += cmdList->VtxBuffer.Size;
			idxMap += cmdList->IdxBuffer.Size;
		}

		vertexBuffer.flush(VK_WHOLE_SIZE);
		indexBuffer.flush(VK_WHOLE_SIZE);
	}

	return updated;
}


/**
 * @brief Updates a buffer of a frame
 * @param frameID
 * @return
*/
bool UIOverlay::updateBuffersOnFrame(const uint32_t& frameID)
{
	bool updated = false;

	ImDrawData* imDrawData = ImGui::GetDrawData();

	if (!imDrawData)
	{
		return false;
	}

	VkDeviceSize vertexBufferSize = imDrawData->TotalVtxCount * sizeof(ImDrawVert);
	VkDeviceSize indexBufferSize = imDrawData->TotalIdxCount * sizeof(ImDrawIdx);

	// Update only if we have new changes in the vtx/idx buffer.
	// No need to update if no changes have been made
	if (vertexBufferSize == 0 || indexBufferSize == 0)
	{
		return false;
	}

	auto& vertexBuffer = vertexBuffers[frameID];
	auto& indexBuffer = indexBuffers[frameID];
	auto& oldVertexCount = oldVertexCounts[frameID];
	auto& oldIndexCount = oldIndexCounts[frameID];

	//  If the buffer was not created or needs to be resized
	if ((vertexBuffer.buffer == VK_NULL_HANDLE) || (oldVertexCount != imDrawData->TotalVtxCount))
	{
		// Update old vertex count so we know how many vertices have been drawn
		// on this frame
		oldVertexCount = imDrawData->TotalVtxCount;

		// Automatically checks if it was mapped so we unmap it for future usage
		// This will not affect the first upload
		vertexBuffer.unmap();
		vertexBuffer.destroy();

		// Creates the buffer to be accessible to the host so we can upload data directly
		vertexBuffer.create(vertexBufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		vertexBuffer.bind();
		vertexBuffer.map(vertexBufferSize);
		updated = true;
	}

	//  If the buffer was not created or needs to be resized
	if ((indexBuffer.buffer == VK_NULL_HANDLE) || (oldIndexCount != imDrawData->TotalIdxCount))
	{
		// Update old index count so we know how many indices have been drawn
		// on this frame
		oldIndexCount = imDrawData->TotalIdxCount;

		// Unmap & destroy the old buffer to reallocate memory
		indexBuffer.unmap();
		indexBuffer.destroy();

		// Proceeds by creating a new index buffer, so we can upload vertices
		indexBuffer.create(indexBufferSize, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		indexBuffer.bind();
		indexBuffer.map(indexBufferSize);
		updated = true;
	}

	// Currently, we know for sure that both buffers are mapped
	ImDrawVert* vtxMap = reinterpret_cast<ImDrawVert*>(vertexBuffer.mapping);
	ImDrawIdx* idxMap = reinterpret_cast<ImDrawIdx*>(indexBuffer.mapping);

	// Imgui command lists are split in multiple command lists
	// so we need to prepare each buffer as necessary
	for (int j = 0; j < imDrawData->CmdListsCount; j++)
	{
		const ImDrawList* cmdList = imDrawData->CmdLists[j];

		// Copy the data from within the current cmd list to the buffers
		memcpy(vtxMap, cmdList->VtxBuffer.Data,
			cmdList->VtxBuffer.Size * sizeof(ImDrawVert));

		memcpy(idxMap, cmdList->IdxBuffer.Data,
			cmdList->IdxBuffer.Size * sizeof(ImDrawIdx));

		// Offset the destination mappings
		vtxMap += cmdList->VtxBuffer.Size;
		idxMap += cmdList->IdxBuffer.Size;
	}

	vertexBuffer.flush(VK_WHOLE_SIZE);
	indexBuffer.flush(VK_WHOLE_SIZE);

	return updated;
}

/**
 * @brief Draw method for a specific frame id
 * @param commandBuffer
 * @param frameID
 * @param framebuffer
*/
void UIOverlay::draw(const VkCommandBuffer& commandBuffer, const uint32_t& frameID, const VkFramebuffer& framebuffer)
{
	// Update the data to draw
	ImDrawData* imDrawData = ImGui::GetDrawData();

	// Update the containing buffers
	updateBuffersOnFrame(frameID);

	// If there's no new data, don't update
	if ((!imDrawData) || (imDrawData->CmdListsCount == 0))
	{
		return;
	}

	ImGuiIO& io = ImGui::GetIO();

	// Binding the pipeline and the descriptor sets
	vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, uiGraphicsPipeline);
	vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, uiGraphicsPipelineLayout, 0, 1, &descriptorSets[frameID], 0, nullptr);

	// Pushing the constants into the vertex shader
	pushConstantsBlock.scale = glm::vec2(2.0f / io.DisplaySize.x, 2.0f / io.DisplaySize.y);
	pushConstantsBlock.translate = glm::vec2(-1.0f - imDrawData->DisplayPos.x * pushConstantsBlock.scale.x, -1.0f - imDrawData->DisplayPos.y * pushConstantsBlock.scale.y);

	vkCmdPushConstants(commandBuffer, uiGraphicsPipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(PushConstantsBlock), &pushConstantsBlock);

	// Binding the vertex and index buffers
	VkDeviceSize offsets = 0;
	vkCmdBindVertexBuffers(commandBuffer, 0, 1, &vertexBuffers[frameID].buffer, &offsets);
	vkCmdBindIndexBuffer(commandBuffer, indexBuffers[frameID].buffer, 0, sizeof(ImDrawIdx) == 2 ? VK_INDEX_TYPE_UINT16 : VK_INDEX_TYPE_UINT32);

	int32_t vertexOffset = 0;
	int32_t indexOffset = 0;

	// Draw indexed in an imgui fashion
	for (int32_t i = 0; i < imDrawData->CmdListsCount; i++)
	{
		const ImDrawList* cmdList = imDrawData->CmdLists[i];
		// For each command buffer within the command list
		for (int32_t j = 0; j < cmdList->CmdBuffer.Size; j++)
		{
			const ImDrawCmd* ptrCmd = &cmdList->CmdBuffer[j];

			// Setting up the scissor for that element (e.g. button)
			VkRect2D scissorRect;
			scissorRect.extent.width =
				static_cast<uint32_t>(ptrCmd->ClipRect.z - ptrCmd->ClipRect.x);
			scissorRect.extent.height =
				static_cast<uint32_t>(ptrCmd->ClipRect.w - ptrCmd->ClipRect.y);

			scissorRect.offset.x =
				std::max(0, static_cast<int32_t>(ptrCmd->ClipRect.x));
			scissorRect.offset.y =
				std::max(0, static_cast<int32_t>(ptrCmd->ClipRect.y));

			vkCmdSetScissor(commandBuffer, 0, 1, &scissorRect);
			vkCmdDrawIndexed(commandBuffer, ptrCmd->ElemCount, 1, indexOffset, vertexOffset, 0);

			// Offset the index buffer start point
			indexOffset += ptrCmd->ElemCount;
		}
		// Offset the vertex buffer start point
		vertexOffset += cmdList->VtxBuffer.Size;
	}
}


/**
 * @brief Front-end new frame
 * @param application
*/
void UIOverlay::newFrame(Application& application)
{
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	static auto startTime = std::chrono::high_resolution_clock::now();

	auto currentTime = std::chrono::high_resolution_clock::now();
	float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

	startTime = currentTime;

	// Updating the camera projection
	application.camera.projection = glm::perspective(glm::radians(45.0f), VKC::get().swapchainExtent.width / static_cast<float>(VKC::get().swapchainExtent.height), 0.1f, 100.f);

	// Init imGui windows and elements
	ImGui::SetNextWindowPos(ImVec2(5, 5), ImGuiCond_Once);
	ImGui::SetNextWindowSizeConstraints(ImVec2(300, 400), ImVec2(400, 500));

	// Create the menu
	ImGui::Begin("Menu");
	{
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

		ImGui::Separator();

		// Create the rendering features sub menu
		if (ImGui::CollapsingHeader("Rendering Features"))
		{
			static bool objectEnabled = true;
			ImGui::Checkbox("Enabled Object", &objectEnabled);
			application.pushConstants.objectEnabled = objectEnabled ? 1 : 0;

			if (!objectEnabled)
			{
				ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
				ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
			}

			static bool texturingEnabled = true;
			ImGui::Checkbox("Enabled Texture", &texturingEnabled);
			application.pushConstants.texturingEnabled = texturingEnabled ? 1 : 0;

			static bool lightingEnabled = true;
			ImGui::Checkbox("Enabled Lighting", &lightingEnabled);
			application.pushConstants.lightingEnabled = lightingEnabled ? 1 : 0;

			if (!objectEnabled)
			{
				ImGui::PopItemFlag();
				ImGui::PopStyleVar();
			}

			ImGui::Separator();
		}

		// Create the actor transform sub menu
		if (ImGui::CollapsingHeader("Actor Transform"))
		{
			ImGui::PushID(0);

			glm::vec3 pos3 = application.duckActor.transform.position;
			glm::vec3 rot3 = application.duckActor.transform.rotation;

			if (!application.pushConstants.objectEnabled)
			{
				ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
				ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
			}

			static bool rotating = false;
			ImGui::Checkbox("Auto Rotate", &rotating);

			if (rotating)
			{
				ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
				ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);

				rot3.z += 30.f * time;
			}

			bool modified = false;
			modified |= ImGui::DragFloat3("Position", glm::value_ptr(pos3), 0.01f);
			modified |= ImGui::DragFloat3("Rotation", glm::value_ptr(rot3), 0.01f);

			if (rotating)
			{
				ImGui::PopItemFlag();
				ImGui::PopStyleVar();
			}

			if (modified || rotating)
			{
				application.duckActor.transform.position = glm::vec4(pos3.x, pos3.y, pos3.z, 1.0f);
				application.duckActor.transform.rotation = glm::vec4(rot3.x, rot3.y, rot3.z, 1.0f);
				application.duckActor.transform.applyTRS();
			}

			if (!application.pushConstants.objectEnabled)
			{
				ImGui::PopItemFlag();
				ImGui::PopStyleVar();
			}


			ImGui::Separator();
			ImGui::PopID();
		}

		// UI For the light transform
		if (ImGui::CollapsingHeader("Light Transform"))
		{
			ImGui::PushID(1);

			glm::vec3 pos3 = application.light.transform.position;
			glm::vec3 rot3 = application.light.transform.rotation;

			bool modified = false;
			modified |= ImGui::DragFloat3("Position", glm::value_ptr(pos3), 0.01f);

			if (modified)
			{
				application.light.transform.position = glm::vec4(pos3.x, pos3.y, pos3.z, 1.0f);
				application.light.transform.applyTRS();
			}

			ImGui::Separator();
			ImGui::PopID();
		}

		// UI For the camera transform
		if (ImGui::CollapsingHeader("Camera Transform"))
		{
			ImGui::PushID(2);

			glm::vec3 pos3 = application.camera.transform.position;

			bool modified = false;
			modified |= ImGui::DragFloat3("Position", glm::value_ptr(pos3), 0.01f);

			if (modified)
			{
				application.camera.transform.position = glm::vec4(pos3.x, pos3.y, pos3.z, 1.0f);
				application.camera.transform.matrix = glm::lookAt(glm::vec3(application.camera.transform.position), glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, -1.0f, 0.0f));
			}

			ImGui::Separator();
			ImGui::PopID();
		}
	}
	ImGui::End();

	//ImGui::ShowDemoWindow();
	// Render to generate draw buffers
	ImGui::Render();
}
